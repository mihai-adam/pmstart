<?php
/**
 * Created by PhpStorm.
 * User: madam
 * Date: 10/15/2015
 * Time: 4:42 PM
 */

namespace Ps;

use GuzzleHttp\Client as GuzzleClient;
use Config;

class Client
{
    public function __construct()
    {

    }

    public function __call($name, $args)
    {
        return call_user_func_array([$this->client, $name], $args);
    }

    public function __get($name)
    {
        return $this->client->$name;
    }
}