<?php
/**
 * Created by PhpStorm.
 * User: madam
 * Date: 2/15/2016
 * Time: 4:57 PM
 */

function _if($confition, $caseTrue, $caseFalse = null)
{
    return ($confition) ? $caseTrue : $caseFalse;
}
