<?php

use Carbon\Carbon;

function elHasError($element, $errors)
{
    $messages = $errors->getMessages();
    return isset($messages[$element]);
}

function elErrors($element, $errors)
{
    if (elHasError($element, $errors)) {
        $messages = $errors->getMessages();
        return $messages[$element];
    }
    return [];
}

function formElementText($name, $label, $value, $config, $errors)
{
    $output = '<div class="form-group ' . _if(elHasError($name, $errors), 'has-error has-feedback') . '">';

    $output .= '<div><label for="' . $name . '" class="control-label">' . $label . ' </label></div>';
    $output .= '<div class="input-group">';
    $output .= '<input ';
    $output .= ' name = "' . $name . '" id = "' . $name . '" type = "text" value = "' . $value . '"  ';
    $output .= ' class = "form-control form-control-sm ' . @_if(isset($config['class']), $config['class']) . '"  ';
    if (isset($config['placeholder'])) {
        $output .= ' placeholder = "' . $config['placeholder'] . '"';
    }
    if (isset($config['readonly'])) {
        $output .= ' readonly = "' . $config['readonly'] . '"';
    }

    $output .= '/>';

    if (isset($config['tooltip'])) {
        $output .= '<div class="input-group-addon tooltip-info"
                        title = "' . $config['tooltip'] . '" >
                    <i class="fa fa-question-circle fa-fw" > </i >
                </div >';
    }

    $output .= '</div>';
    if (elHasError($name, $errors)) {
        foreach (elErrors($name, $errors) as $message) {
            $output .= '<p class="help-block"><spnan class="text-danger">' . $message . '</spnan></p>';
        }
    }

    $output .= '</div>';
    return $output;

}

function formElementSelect($name, $label, $value = null, $options, $config, $errors, $blank = false)
{
    $output = '<div class="form-group ' . _if(elHasError($name, $errors), 'has-error has-feedback') . '">';
    $output .= '<div><label for="' . $name . '" class="control-label">' . $label . ' </label></div>';
    $output .= '<div class="input-group">';
    $output .= '<select ';
    $output .= ' name = "' . $name . '" id = "' . $name . '" ';
    $output .= ' class = "form-control form-control-sm ' . @_if(isset($config['class']), $config['class']) . '"  >';
    if ($blank) {
        $output .= '<option value="" >Please select</option>';
    }
    if (isset($config['sort']) && $config['sort']) {
        asort($options);
    }
    foreach ($options as $id => $element) {
        if (is_array($element)) {
            $label = $element['name'];
            unset($element['name']);
            $output .= '<option value="' . $id . '" ' . @_if(($id == $value), ' selected = "selected"') . ' ';
            foreach ($element as $data => $item) {
                if (is_string($item) || is_int($item)) {
                    $output .= ' ' . $data . '="' . $item . '" ';
                }
            }
            $output .= '>' . $label . '</option>';
        } else {
            $output .= '<option value="' . $id . '" ' . _if(($id == $value), ' selected = "selected"') . '>' . $element . '</option>';
        }

    }
    $output .= '</select>';
    if (isset($config['tooltip'])) {
        $output .= '<div class="input-group-addon tooltip-info"
                        title = "' . $config['tooltip'] . '" >
                    <i class="fa fa-question-circle fa-fw" > </i >
                </div >';
    }

    $output .= '</div>';
    if (elHasError($name, $errors)) {
        foreach (elErrors($name, $errors) as $message) {
            $output .= '<p class="help-block"><spnan class="text-danger">' . $message . '</spnan></p>';
        }
    }
    $output .= '</div>';
    return $output;

}

function formElementTextarea($name, $label, $value, $config, $errors)
{
    $output = '<div class="form-group ' . _if(elHasError($name, $errors), 'has-error has-feedback') . '">';

    $output .= '<div><label for="' . $name . '" class="control-label">' . $label . ' </label></div>';
    $output .= '<div class="">';
    $output .= '<textarea ';
    $output .= ' name = "' . $name . '" id = "' . $name . '"  ';
    $output .= ' class = "form-control form-control-sm ' . @_if(isset($config['class']), $config['class']) . '"  ';
    if (isset($config['placeholder'])) {
        $output .= ' placeholder = "' . $config['placeholder'] . '"';
    }
    if (isset($config['readonly'])) {
        $output .= ' readonly = "' . $config['readonly'] . '"';
    }
    if (isset($config['rows'])) {
        $output .= ' rows = "' . $config['rows'] . '"';
    }

    $output .= '>' . $value . '</textarea>';

    $output .= '</div>';
    if (elHasError($name, $errors)) {
        foreach (elErrors($name, $errors) as $message) {
            $output .= '<p class="help-block"><spnan class="text-danger">' . $message . '</spnan></p>';
        }
    }

    $output .= '</div>';
    return $output;
}

function formElementPassword($name, $label, $value, $config, $errors)
{
    $output = '<div class="form-group ' . _if(elHasError($name, $errors), 'has-error has-feedback') . '">';

    $output .= '<div><label for="' . $name . '" class="control-label">' . $label . ' </label></div>';
    $output .= '<div class="input-group">';
    $output .= '<input ';
    $output .= ' name = "' . $name . '" id = "' . $name . '" type = "password" value = "' . $value . '"  ';
    $output .= ' class = "form-control form-control-sm ' . @_if(isset($config['class']), $config['class']) . '"  ';
    if (isset($config['placeholder'])) {
        $output .= ' placeholder = "' . $config['placeholder'] . '"';
    }
    if (isset($config['readonly'])) {
        $output .= ' readonly = "' . $config['readonly'] . '"';
    }

    $output .= '/>';

    if (isset($config['tooltip'])) {
        $output .= '<div class="input-group-addon tooltip-info"
                        title = "' . $config['tooltip'] . '" >
                    <i class="fa fa-question-circle fa-fw" > </i >
                </div >';
    }

    $output .= '</div>';
    if (elHasError($name, $errors)) {
        foreach (elErrors($name, $errors) as $message) {
            $output .= '<p class="help-block"><spnan class="text-danger">' . $message . '</spnan></p>';
        }
    }

    $output .= '</div>';
    return $output;

}


function formdate($date)
{
    $input = new Carbon($date);
    return $input->format('Y/m/d');
}