<?php
/**
 * Created by PhpStorm.
 * User: madam
 * Date: 2/16/2016
 * Time: 2:21 PM
 */

function _components($name)
{
    $output = [];
    foreach (config('component.' . $name) as $component) {
        $output[$component] = trans('component.' . $name . '.' . $component);
    }
    return $output;
}

function dependencies()
{
    $output = [];
    foreach (config('component.category') as $dependency => $category) {
        $output[$dependency] = trans('component.category.dependencies.' . $dependency);
    }
    return $output;
}

function categories($json = false)
{
    $output = [];
    foreach (config('component.category') as $dependency => $category) {
        foreach ($category as $c) {
            $output[$c] = [
                'name' => trans('component.category.' . $c),
                'data-related-dependency' => $dependency,
                'value' => $c
            ];
        }
    }
    return ($json) ? json_encode($output) : $output;
}

function phases()
{
    return _components('phase');
}

function rags()
{
    $output = [];
    foreach (config('component.rag') as $component) {
        $output[$component] = trans('component.color.'. $component);
    }
    return $output;
}

function lmhs()
{
    return _components('lmh');
}

function raids()
{
    return _components('raid');
}
