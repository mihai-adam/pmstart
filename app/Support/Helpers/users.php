<?php
/**
 * Created by PhpStorm.
 * User: madam
 * Date: 2/17/2016
 * Time: 11:23 AM
 */

use App\Repositories\UserRepository;
use App\Models\User;

function superusers()
{
    $users = UserRepository::instance();
    return UserRepository::exportList($users->getAll(User::USER_ROLE_SUPERUSER));
}

function admins()
{
    $users = UserRepository::instance();
    return UserRepository::exportList($users->getAll(User::USER_ROLE_ADMINISTRATOR));
}

function csms()
{
    $users = UserRepository::instance();
    return UserRepository::exportList($users->getAll(User::USER_ROLE_CSM));
}

function pms()
{
    $users = UserRepository::instance();
    return UserRepository::exportList($users->getAll(User::USER_ROLE_PM));
}

function managers()
{
    $csms = csms();
    $pms = pms();
    dd($csms+$pms);
}

function roles($superuser = false)
{
    $output = [];
    $data = UserRepository::getAllRoles($superuser);
    foreach ($data as $e) {
        $output[$e] = trans('component.role.' . $e);
    }
    return $output;
}
