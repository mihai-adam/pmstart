<?php

use App\Models\Customers;


function customers()
{
    $customers = Customers::orderBy('name', 'ASC')->get();
    $output = [];
    foreach ($customers as $customer) {
        $output[$customer->id] = $customer->name;
    }
    return $output;
}