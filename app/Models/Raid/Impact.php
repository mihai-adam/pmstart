<?php

namespace App\Models\Raid;

use Illuminate\Database\Eloquent\Model;

class Impact extends Model
{
    protected $table = 'impacts';
}
