<?php

namespace App\Models;

use App\User as BaseUser;
use Carbon\Carbon;


class User extends BaseUser
{
    const USER_ROLE_SUPERUSER = 'super';
    const USER_ROLE_ADMINISTRATOR = 'admin';
    const USER_ROLE_CSM = 'csm';
    const USER_ROLE_PM = 'pm';

    public function getStatusAttribute()
    {
        if (isset($this->attributes['deleted_at'])) {
            return (empty($this->attributes['deleted_at'])) ? 'active' : 'suspended';
        }
        return 'active';
    }

    public function setStatusAttribute($value)
    {
        if('suspended' == $value){

            $this->attributes['deleted_at'] = self::now();
        }
    }
}
