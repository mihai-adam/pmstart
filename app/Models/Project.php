<?php
/**
 * Created by PhpStorm.
 * User: madam
 * Date: 10/14/2015
 * Time: 5:41 PM
 */

namespace App\Models;

use App\Models\Project\Delay;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends Base
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'projects';


    public function customer()
    {
        return $this->belongsTo(Customers::class);
    }

    public function pm()
    {
        return $this->belongsTo(User::class);
    }

    public function csm()
    {
        return $this->belongsTo(User::class);
    }

    public function delays()
    {
        $relation = $this->hasMany(Delay::class);
        return $relation->orderBy('created_at', 'DESC')->orderBy('id', 'DESC');
    }

    public function getModifiedEndDateAttribute()
    {
        $estimation = new Carbon($this->attributes['estimated_at']);
        return $estimation->addWeekdays($this->delays->sum('days'))->format(self::DATE_MASK);
    }


    public function risks()
    {
        $relation = $this->hasMany(Raid::class);
        return $relation->where('type', 'risk');
    }

    public function actions()
    {
        $relation = $this->hasMany(Raid::class);
        return $relation->where('type', 'action');
    }

    public function issues()
    {
        $relation = $this->hasMany(Raid::class);
        return $relation->where('type', 'issue');
    }

    public function dependencies()
    {
        $relation = $this->hasMany(Raid::class);
        return $relation->where('type', 'dependency');
    }


}