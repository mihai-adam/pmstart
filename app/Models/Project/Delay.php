<?php

namespace App\Models\Project;

use App\Models\Project;
use App\Models\Base;
use Carbon\Carbon;

class Delay extends Base
{
    protected $table = 'project_delay';

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

   
}
