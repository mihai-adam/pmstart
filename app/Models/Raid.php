<?php

namespace App\Models;

use App\Models\Raid\Impact;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Raid extends Model
{

    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'raid';

    public function impact()
    {
        return $this->belongsTo(Impact::class);
    }

    public function author()
    {
        return $this->belongsTo(User::class);
    }

    public function owner()
    {
        return $this->belongsTo(User::class);
    }

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function getClassAttribute()
    {
        $output = '';
        switch ($this->attributes['rag']) {
            case 'red':
                $output = 'danger';
                break;
            case 'amber':
                $output = 'warning';
                break;
            case 'green':
                $output = 'success';
                break;
        }
        return $output;
    }
}
