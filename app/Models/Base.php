<?php
/**
 * Created by PhpStorm.
 * User: madam
 * Date: 10/14/2015
 * Time: 5:41 PM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Base extends Model
{

    const DATETIME_MASK = 'Y-m-d H:m:s';
    const DATE_MASK = 'd-m-Y';

    public static function now()
    {
        $time = Carbon::now();
        return $time->format(self::DATETIME_MASK);
    }

    public function date($attribute)
    {
        $date = new Carbon($this->attributes[$attribute]);
        return $date->format(self::DATE_MASK);
    }
}