<?php

namespace App\Policies;

use App\Repositories\UserRepository;
use Illuminate\Auth\Access\Gate;
use App\Models\User;

class AppPolicy
{

    private $gate;

    public static function register(Gate $gate)
    {
        $instance = new self($gate);
        $instance->setup();
    }

    public function __construct(Gate $gate)
    {
        $this->gate = $gate;
    }

    public function setup()
    {
        $gate = $this->gate;

        $gate->define('manage-users', function () {
            $my = UserRepository::myProfile()->model();
            return UserRepository::canManageUsers($my);
        });
    }


}
