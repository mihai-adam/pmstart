<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/', ['uses' => 'ScreenController@index', 'as' => 'screen.dashboard']);
Route::get('/error', ['uses' => 'ScreenController@error', 'as' => 'screen.error']);

Route::group(['middleware' => 'guest', 'prefix' => 'user', 'as' => 'user.'], function () {
    Route::get('login', ['as' => 'login', 'uses' => 'UserController@login']);
    Route::post('login', ['as' => 'authenticate', 'uses' => 'UserController@authenticate']);
    Route::get('logout', ['as' => 'logout', 'uses' => 'UserController@logout']);
});

Route::group(['middleware' => 'auth'], function () {
    Route::group(['prefix' => 'user', 'as' => 'user.'], function () {
        Route::get('', ['as' => 'index', 'uses' => 'UserController@index']);
        Route::get('create', ['as' => 'create', 'uses' => 'UserController@create']);
        Route::post('', ['as' => 'store', 'uses' => 'UserController@store']);
        Route::get('{id}/show', ['as' => 'show', 'uses' => 'UserController@show']);
        Route::get('{id}/edit', ['as' => 'edit', 'uses' => 'UserController@edit']);
        Route::post('{id}', ['as' => 'update', 'uses' => 'UserController@update']);
        Route::get('logout', ['as' => 'logout', 'uses' => 'UserController@logout']);
        Route::get('profile', ['as' => 'editMyProfile', 'uses' => 'UserController@editMyProfile']);
        Route::post('profile', ['as' => 'updateMyProfile', 'uses' => 'UserController@updateMyProfile']);
        Route::get('password', ['as' => 'editPassword', 'uses' => 'UserController@editPassword']);
        Route::post('password', ['as' => 'updatePassword', 'uses' => 'UserController@updatePassword']);
    });
    Route::group(['prefix' => 'project', 'as' => 'project.'], function () {
        Route::get('', ['as' => 'index', 'uses' => 'ProjectController@index']);
        Route::get('create', ['as' => 'create', 'uses' => 'ProjectController@create']);
        Route::get('{id}/edit', ['as' => 'edit', 'uses' => 'ProjectController@edit']);
        Route::get('{id}/show', ['as' => 'show', 'uses' => 'ProjectController@show']);
        Route::put('{id}', ['as' => 'update', 'uses' => 'ProjectController@update']);
        Route::post('', ['as' => 'store', 'uses' => 'ProjectController@store']);
        Route::group(['prefix' => '{id}/raid', 'as' => 'raid.'], function () {
            Route::get('create/{type}', ['as' => 'create', 'uses' => 'RaidController@create']);
            Route::get('edit/{raid}', ['as' => 'edit', 'uses' => 'RaidController@edit']);
            Route::get('rag/{raid}/{rag}', ['as' => 'rag', 'uses' => 'RaidController@rag']);
            Route::get('priority/{raid}/{priority}', ['as' => 'priority', 'uses' => 'RaidController@priority']);
            Route::post('', ['as' => 'store', 'uses' => 'RaidController@store']);
            Route::put('{raid}', ['as' => 'update', 'uses' => 'RaidController@update']);
            Route::delete('{raid}', ['as' => 'destroy', 'uses' => 'RaidController@destroy']);
        });
        Route::group(['prefix' => '{id}/delay', 'as' => 'delay.'], function () {
            Route::get('create', ['as' => 'create', 'uses' => 'Project\DelayController@create']);
            Route::post('', ['as' => 'store', 'uses' => 'Project\DelayController@store']);
        });
    });
});

Route::controller('/sample', 'SampleController');