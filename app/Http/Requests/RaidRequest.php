<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class RaidRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $method = 'get' . ucfirst($this->request->get('type')) . 'Rules';
        if (method_exists($this, $method)) {
            return call_user_func([$this, $method]);
        } else {
            return [
                'type' => 'required|in:' . implode(',', array_keys(raids())),
                'rag' => 'required|in:' . implode(',', array_keys(rags())),
                'probability' => 'required|in:' . implode(',', array_keys(lmhs())),
                'level' => 'required|in:' . implode(',', array_keys(lmhs())),
                'raised_at' => 'required|date_format:Y/m/d',
                'impact' => 'required|min:3|max:256',
                'author_id' => 'required|exists:users,id',
                'owner_id' => 'required|exists:users,id',
                'estimated_at' => 'required|date_format:Y/m/d',
                'description' => 'required|min:3|max:256',
                'mitigation' => 'required|min:3|max:256',
            ];
        }
    }

    private function getActionRules()
    {
        return [
            'type' => 'required|in:' . implode(',', array_keys(raids())),
            'priority' => 'required|in:' . implode(',', array_keys(lmhs())),
            'author_id' => 'required|exists:users,id',
            'owner_id' => 'required|exists:users,id',
            'estimated_at' => 'required|date_format:Y/m/d',
            'description' => 'required|min:3|max:256',
            'mitigation' => 'min:3|max:256',
        ];
    }
}
