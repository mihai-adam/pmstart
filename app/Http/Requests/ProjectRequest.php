<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProjectRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'phase' => 'required',
            'customer_id' => 'required',
            'pm_id' => 'required|integer',
            'csm_id' => 'required|integer',
            'rag' => 'required',
            'code' => 'required|max:11',
            'complete' => 'required|integer|max:100|min:0',
            'started_at' => 'required|date_format:Y/m/d',
            'estimated_at' => 'required|date_format:Y/m/d',
            'client_contact' => 'email|max:64'
        ];
    }
}
