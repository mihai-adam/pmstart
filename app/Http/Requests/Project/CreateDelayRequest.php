<?php

namespace App\Http\Requests\Project;

use App\Http\Requests\Request;

class CreateDelayRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [
            'days' => 'required|integer|min:1',
            'dependency' => 'in:' . implode(',', array_merge(['none'], array_keys(dependencies()))),
            'category' => 'in:' . implode(',', array_keys(categories())),
            'reason' => 'required|min:3|max:1024',
        ];
        return $rules;
    }
}
