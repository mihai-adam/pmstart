<?php

namespace App\Http\Requests\User;

use App\Http\Requests\Request;
use App\Repositories\UserRepository;
use Hash;

class UpdatePasswordRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'current' => 'required',
            'password' => 'required|confirmed',
            'password_confirmation' => 'required'
        ];
    }

    public function validate()
    {
        $instance = $this->getValidatorInstance();

        if (!$this->passesAuthorization()) {
            $this->failedAuthorization();
        } elseif (!$instance->passes()) {
            $this->failedValidation($instance);
        } else {
            $user = UserRepository::myProfile();
            if (!Hash::check($this->get('current'), $user->password)) {
                $instance->errors()->add('current', 'wrong password');
                $this->failedValidation($instance);
            }
        }
    }
}
