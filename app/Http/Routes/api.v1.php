<?php

return;
Route::get('/', function () {
    return view('welcome');
});

Route::resource('projects', 'Api\V1\ProjectController');
Route::resource('ranks', 'Api\V1\RankController');
Route::resource('categories', 'Api\V1\CategoryController');
Route::resource('phases', 'Api\V1\PhaseController');
Route::resource('categories', 'Api\V1\CategoryController');
Route::resource('types', 'Api\V1\TypeController');