<?php

namespace App\Http\Controllers;

use App\Repositories\UserRepository;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use Gate;
use Validator;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\User\LoginRequest;
use App\Http\Requests\User\UpdateMyProfileRequest;
use App\Http\Requests\User\UpdatePasswordRequest;
use App\Http\Requests\User\CreateProfileRequest;
use App\Http\Requests\User\UpdateProfileRequest;

class UserController extends Controller
{
//    use AuthenticatesAndRegistersUsers, ThrottlesLogins;
    use ThrottlesLogins;


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $repo = new UserRepository();
        $model = $repo->model();
        $users = $model->where('role', '!=', 'super')->orderBy('name', 'ASC')->get();
        return view('pages.user.index', [
            'users' => $users
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('manage-users')) {
            return redirect()->route('screen.error');
        }
        $repo = new UserRepository();
        $user = $repo->model();
        return view('pages.user.form', [
            'user' => $user,
            'action' => route('user.store'),
            'superuser' => false
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateProfileRequest $request)
    {
        $repo = new UserRepository();
        $user = $repo->model();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->role = $request->role;
        $user->password = Hash::make('1234');
        $user->save();
        return redirect()->route('user.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('manage-users')) {
            return redirect()->route('screen.error');
        }

        try {
            $user = UserRepository::findOrFail($id);
        } catch (\Exception $e) {
            return redirect()->route('screen.error');
        }

        return view('pages.user.form', [
            'user' => $user,
            'action' => route('user.update', ['id' => $user->id]),
            'superuser' => true
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProfileRequest $request, $id)
    {
        if (Gate::denies('manage-users')) {
            return redirect()->route('screen.error');
        }

        try {
            $user = UserRepository::findOrFail($id);
        } catch (\Exception $e) {
            return redirect()->route('screen.error');
        }
        $user->name = $request->name;
        $user->email = $request->email;
        $user->role = $request->role;
        $user->save();
        if ('suspended' == $request->status) {
            $user->delete();
        }
        return redirect()->route('user.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function login()
    {
        return view('pages.user.login');
    }


    public function authenticate(LoginRequest $request)
    {

        if ($this->hasTooManyLoginAttempts($request)) {
            return $this->sendLockoutResponse($request);
        }


        if (Auth::attempt($request->only('email', 'password'), $request->has('remember'))) {
            $this->clearLoginAttempts($request);
            return redirect()->route('screen.dashboard');
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);


        return redirect()->route('user.login')
            ->withInput($request->only('email', 'remember'))
            ->withErrors([
                'email' => 'Wrong email or password',
            ]);
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('user.login');
    }

    protected function loginUsername()
    {
        return 'email';
    }

    public function editMyProfile()
    {
        $user = UserRepository::myProfile();
        return view('pages.user.form', [
            'user' => $user,
            'action' => route('user.editMyProfile'),
            'superuser' => false
        ]);
    }

    public function updateMyProfile(UpdateMyProfileRequest $request)
    {
        $profile = UserRepository::myProfile()->model();
        $profile->name = $request->name;
        $profile->email = $request->email;
        $profile->save();
        return redirect()->route('screen.dashboard');
    }

    public function editPassword()
    {
        return view('pages.user.form.password');
    }

    public function updatePassword(UpdatePasswordRequest $request)
    {

        $profile = UserRepository::myProfile()->model();
        $profile->password = Hash::make($request->password);
        $profile->save();
        return redirect()->route('screen.dashboard');
    }
}
