<?php

namespace App\Http\Controllers;

use App\Models\Raid\Impact;

use App\Http\Requests;
use App\Repositories\ProjectRepository;
use App\Repositories\RaidRepository;
use App\Http\Requests\RaidRequest;

class RaidController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($project, $action)
    {
        $pRepo = new ProjectRepository();
        $project = $pRepo->find($project);
        $rRepo = new RaidRepository();
        $raid = $rRepo->model();
        $raid->rag = old('rag');
        $raid->probability = old('probability');
        $raid->author_id = old('author_id');
        $raid->owner_id = old('owner_id');
        $raid->description = old('description');
        $raid->mitigation = old('mitigation');

        $raid->type = $action;
        $impact = new Impact();
        $impact->level = old('level');
        $impact->raised_at = old('raised_at');
        $impact->description = old('impact');
        $raid->impact = $impact;
        return view('pages.project.raid.form.' . $action, [
            'project' => $project,
            'raid' => $raid,
            'form' => [
                'url' => route('project.raid.store', ['id' => $project->id]),
                'method' => 'post',
                'name' => 'Create new <span style="text-transform: lowercase">' . trans('component.raid.' . $action) . '</span> for ' . $project->name
            ]
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(RaidRequest $request, $project)
    {
        $rRepo = new RaidRepository();
        $raid = $rRepo->model();
        $raid->project_id = $project;

        $raid->type = $request->type;
        $raid->rag = $request->rag;
        $raid->priority = $request->priority;
        $raid->probability = $request->probability;
        $raid->author_id = $request->author_id;
        $raid->owner_id = $request->owner_id;
        $raid->description = $request->description;
        $raid->mitigation = $request->mitigation;
        $raid->estimated_at = $request->estimated_at;

        if (null != $request->level) {
            $impact = new Impact();
            $impact->level = $request->level;
            $impact->raised_at = $request->raised_at;;
            $impact->description = $request->description;
            $impact->save();
            $raid->impact_id = $impact->id;
        }
        if ('action' == $request->type) {
            $raid->rag = $this->getRagByPriority($request->priority);
        }
        try {
            $raid->save();
            $raid->project->touch();
            return redirect()->route('project.show', ['id' => $project]);
        } catch (\Exception $e) {
            return redirect()->route('screen.error')->withErrors(['errors' => $e->getMessage()]);
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($project, $raid)
    {

        $rRepo = new RaidRepository();
        $raid = $rRepo->model()->withTrashed()->find($raid);
        if (!empty(old())) {
            $raid->rag = old('rag');
            $raid->probability = old('probability');
            $raid->author_id = old('author_id');
            $raid->owner_id = old('owner_id');
            $raid->description = old('description');
            $raid->mitigation = old('mitigation');
            $raid->impact->level = old('level');
            $raid->impact->raised_at = old('raised_at');
            $raid->impact->description = old('impact');
            $raid->impact->description = old('impact');
            $raid->estimated_at = old('estimated_at');
        }

        return view('pages.project.raid.form.' . $raid->type, [
            'project' => $raid->project,
            'raid' => $raid,
            'form' => [
                'url' => route('project.raid.update', [
                    'id' => $raid->project_id,
                    'raid' => $raid
                ]),
                'method' => 'put',
                'name' => 'Edit ' . trans('component.raid.' . $raid->type) . ' for ' . $raid->project->name
            ]
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(RaidRequest $request, $project, $raid)
    {
//        dd($request->all());
        $rRepo = new RaidRepository();
        $raid = $rRepo->model()->withTrashed()->find($raid);
        $raid->rag = $request->rag;
        $raid->priority = $request->priority;
        $raid->probability = $request->probability;
        $raid->author_id = $request->author_id;
        $raid->owner_id = $request->owner_id;
        $raid->description = $request->description;
        $raid->mitigation = $request->mitigation;
        $raid->estimated_at = $request->estimated_at;
        if (null != $request->level) {
            $raid->impact->level = $request->level;
            $raid->impact->raised_at = $request->raised_at;;
            $raid->impact->description = $request->impact;
            $raid->impact->save();
        }
        if ('action' == $request->type) {
            $raid->rag = $this->getRagByPriority($request->priority);
        }
        try {

            $raid->save();
            $raid->project->touch();
            return redirect()->route('project.show', ['id' => $project]);
        } catch (\Exception $e) {
            return redirect()->route('screen.error')->withErrors(['errors' => $e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($project, $raid)
    {
        $rRepo = new RaidRepository();
        $raid = $rRepo->model()->withTrashed()->find($raid);
        $raid->project->touch();
        $raid->delete();
        return redirect()->route('project.show', ['id' => $project]);
    }

    public function rag($project, $raid, $rag)
    {
        if (array_key_exists($rag, rags())) {
            $rRepo = new RaidRepository();
            $raid = $rRepo->model()->withTrashed()->find($raid);
            $raid->rag = $rag;
            try {
                $raid->save();
                $raid->project->touch();
                return redirect()->route('project.show', ['id' => $project]);
            } catch (\Exception $e) {
                return redirect()->route('screen.error')->withErrors(['errors' => $e->getMessage()]);
            }
        } else {
            return redirect()->route('screen.error')->withErrors(['errors' => ['rag' => 'Invalid rag specified']]);
        }

    }

    public function priority($project, $raid, $priority)
    {
        if (array_key_exists($priority, lmhs())) {
            $rRepo = new RaidRepository();
            $raid = $rRepo->model()->withTrashed()->find($raid);
            $raid->rag = $this->getRagByPriority($priority);
            $raid->priority = $priority;
            try {
                $raid->save();
                $raid->project->touch();
                return redirect()->route('project.show', ['id' => $project]);
            } catch (\Exception $e) {
                return redirect()->route('screen.error')->withErrors(['errors' => $e->getMessage()]);
            }
        } else {
            return redirect()->route('screen.error')->withErrors(['errors' => ['rag' => 'Invalid rag specified']]);
        }

    }

    private function getRagByPriority($priority)
    {
        $map = [
            'low' => 'green',
            'medium' => 'amber',
            'high' => 'red'
        ];
        return $map[$priority];
    }
}
