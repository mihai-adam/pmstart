<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class SampleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getDashboard()
    {
        return view('pages.samples.dashboard');
    }

    public function getBlank()
    {
        return view('pages.samples.blank');
    }

    public function getButtons()
    {
        return view('pages.samples.buttons');
    }

    public function getFlot()
    {
        return view('pages.samples.flot');
    }


    public function getForms()
    {
        return view('pages.samples.forms');
    }

    public function getGrid()
    {
        return view('pages.samples.grid');
    }

    public function getIcons()
    {
        return view('pages.samples.icons');
    }

    public function getIndex()
    {
        return view('pages.samples.dashboard');
    }

    public function getLogin()
    {
        return view('pages.samples.login');
    }

    public function getMorris()
    {
        return view('pages.samples.morris');
    }

    public function getNotifications()
    {
        return view('pages.samples.notifications');
    }

    public function getPanelsWells()
    {
        return view('pages.samples.panels-wells');
    }

    public function getTables()
    {
        return view('pages.samples.tables');
    }

    public function getTypography()
    {
        return view('pages.samples.typography');
    }


}
