<?php

namespace App\Http\Controllers\Project;

use App\Models\Project;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Project\Delay;
use App\Http\Requests\Project\CreateDelayRequest;

class DelayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($projecyId)
    {
        $delay = new Delay();
        $delay->project_id = $projecyId;
        $delay->dependency = old('dependency');
        $delay->category = old('category');
        $delay->reason = old('phase');
        $delay->days = old('days');

        return view('pages.project.delay.form', [
            'delay' => $delay,
            'form' => [
                'url' => route('project.delay.store', ['id' => $projecyId]),
                'method' => 'post',
                'name' => 'Delay project'
            ]
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store($projectId, CreateDelayRequest $request)
    {
        $delay = new Delay();
        $delay->project_id = $projectId;
        $delay->days = $request->days;
        $delay->dependency = $request->dependency;
        $delay->category = $request->category;
        $delay->reason = $request->reason;
        try {
            $delay->save();
            $delay->project->touch();
            return redirect()->route('project.show', [$projectId]);
        } catch (\Exception $e) {
            return redirect()->route('screen.error')->withErrors(['errors' => $e->getMessage()]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
