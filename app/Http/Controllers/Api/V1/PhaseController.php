<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\ComponentRepository;
use Response;
use Exception;

class PhaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $component = new ComponentRepository();
        return Response::json($component->getAllPhases());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $component = new ComponentRepository();
        $status = "error";
        try {
            $response = $component->createPhase($request->all());
            $status = "success";
        } catch (Exception $e) {
            $response = $e->getMessage();
        }
        return Response::json([
            'status' => $status,
            'response' => $response
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $component = new ComponentRepository();
        return Response::json($component->getPhaseId($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $component = new ComponentRepository();
        $phase = $component->getPhaseId($id);
        $status = "error";
        if (null != $phase) {
            try {
                $response = $component->update($phase, $request->all());
                $status = "success";
            } catch (Exception $e) {
                $response = $e->getMessage();
            }
        } else {
            $response = "Specified phase is no longer available";
        }
        return Response::json([
            'status' => $status,
            'response' => $response
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $component = new ComponentRepository();
        $phase = $component->getPhaseId($id);
        $status = "error";
        if (null != $phase) {
            try {
                $response = $phase->delete();
                $status = "success";
            } catch (Exception $e) {
                $response = $e->getMessage();
            }
        } else {
            $response = "Specified phase is no longer available";
        }
        return Response::json([
            'status' => $status,
            'response' => $response
        ]);

    }
}
