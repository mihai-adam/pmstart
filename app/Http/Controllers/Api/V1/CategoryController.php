<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\CategoryRepository;
use Response;
use Exception;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $model = new CategoryRepository();
        return Response::json($model->getAll());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $model = new CategoryRepository();
        $status = "error";
        try {
            $response = $model->create($request->all());
            $status = "success";
        } catch (Exception $e) {
            $response = $e->getMessage();
        }
        return Response::json([
            'status' => $status,
            'response' => $response
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = new CategoryRepository();
        return Response::json($model->getId($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = new CategoryRepository();
        $status = "error";
        try {
            $response = $model->update($id, $request->all());
            $status = "success";
        } catch (Exception $e) {
            $response = $e->getMessage();
        }
        return Response::json([
            'status' => $status,
            'response' => $response
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = new ComponentRepository();
        $category = $model->getCategoryId($id);
        $status = "error";
        if (null != $category) {
            try {
                $response = $category->delete();
                $status = "success";
            } catch (Exception $e) {
                $response = $e->getMessage();
            }
        } else {
            $response = "Specified category is no longer available";
        }
        return Response::json([
            'status' => $status,
            'response' => $response
        ]);

    }
}
