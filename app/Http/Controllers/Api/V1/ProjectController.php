<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Project;
use Response;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = ["type_id" => "2",
            "phase_id" => "2",
            "customer_id" => "5",
            "pm_id" => "muhammet.alay@monitise.com",
            "rank_id" => "2",
            "name" => "sss",
            "code" => "ssss",
            "complete" => "0",
            "reason" => "sss",
            "started_at" => "14/10/2015",
            "estimated_at" => "23/10/2015",
            "finished_at" => "30/10/2015",
            "updated_at" => "2015-10-14 15:16:48",
            "created_at" => "2015-10-14 15:16:48"];
        $project = new Project();
        try {
            $project->insert($request->all());
            return Response::json($project);
        } catch (\Exception $e) {
            return Response::json([
                'error' => $e->getMessage(),
                'project' => $project
            ]);
        }


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $project = new Project();

        $project->type_id = $request->get('type_id');
        $project->phase_id = $request->get('phase_id');
        $project->customer_id = $request->get('customer_id');
        $project->pm_id = $request->get('pm_id');
        $project->rank_id = $request->get('rank_id');
        $project->name = $request->get('name');
        $project->code = $request->get('code');
        $project->complete = $request->get('complete');
        $project->reason = $request->get('reason');
        $project->started_at = $request->get('started_at');
        $project->estimated_at = $request->get('estimated_at');
        $project->finished_at = $request->get('finished_at');
//        dd($project);
        try {
            $project->save();
            return Response::json($project);
        } catch (\Exception $e) {
            return Response::json([
                'error' => $e->getMessage(),
                'project' => $project
            ]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
