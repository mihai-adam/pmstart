<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\ComponentRepository;
use Response;
use Exception;

class RankController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $component = new ComponentRepository();
        return Response::json($component->getAllRanks());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $component = new ComponentRepository();
        $status = "error";
        try {
            $response = $component->createRank($request->all());
            $status = "success";
        } catch (Exception $e) {
            $response = $e->getMessage();
        }
        return Response::json([
            'status' => $status,
            'response' => $response
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $component = new ComponentRepository();
        return Response::json($component->getRankId($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $component = new ComponentRepository();
        $rank = $component->getRankId($id);
        $status = "error";
        if (null != $rank) {
            try {
                $response = $component->update($rank, $request->all());
                $status = "success";
            } catch (Exception $e) {
                $response = $e->getMessage();
            }
        } else {
            $response = "Specified rank is no longer available";
        }
        return Response::json([
            'status' => $status,
            'response' => $response
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $component = new ComponentRepository();
        $rank = $component->getRankId($id);
        $status = "error";
        if (null != $rank) {
            try {
                $response = $rank->delete();
                $status = "success";
            } catch (Exception $e) {
                $response = $e->getMessage();
            }
        } else {
            $response = "Specified rank is no longer available";
        }
        return Response::json([
            'status' => $status,
            'response' => $response
        ]);

    }
}
