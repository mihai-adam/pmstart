<?php

namespace App\Http\Controllers;


use App\Http\Requests;
use App\Http\Requests\ProjectRequest;
use App\Repositories\ProjectRepository;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $repo = new ProjectRepository;
        $projects = $repo->orderBy('id', 'DESC')
            ->with('pm')
            ->with('csm')
            ->with('customer')
            ->with(['risks' => function ($q) {
                return $q->withTrashed();
            }])
            ->with('actions')
            ->with(['issues' => function ($q) {
                return $q->withTrashed();
            }])
            ->with('dependencies')
            ->with('delays')
            ->withTrashed()
            ->orderBy('code', 'DESC')
            ->get();
        return view('pages.project.index', [
            'projects' => $projects
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $project = new ProjectRepository();
        $project->customer_id = old('customer_id');
        $project->pm_id = old('pm_id');
        $project->csm_id = old('csm_id');
        $project->name = old('name');
        $project->code = old('code');
        $project->dependency = old('dependency');
        $project->category = old('category');
        $project->phase = old('phase');
        $project->rag = old('rag');
        $project->complete = old('complete');
        $project->started_at = old('started_at');
        $project->estimated_at = old('estimated_at');
        $project->actions_lw = old('actions_lw');
        $project->actions_nw = old('actions_nw');

        return view('pages.project.form', [
            'project' => $project,
            'return' => route('project.index'),
            'form' => [
                'url' => route('project.store'),
                'method' => 'post',
                'name' => 'Create new project',
            ]
        ]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $repo = new ProjectRepository();
        $project = $repo->with(['risks' => function ($q) {
            return $q->with(['author', 'owner', 'impact'])->withTrashed();
        }])->with(['actions' => function ($q) {
            return $q->with(['author', 'owner', 'impact'])->withTrashed();
        }])->with(['issues' => function ($q) {
            return $q->with(['author', 'owner', 'impact'])->withTrashed();
        }])->with(['dependencies' => function ($q) {
            return $q->with(['author', 'owner', 'impact'])->withTrashed();
        }])->with(['pm', 'csm', 'customer', 'delays'])->find($id);
        if (null != $project->type_id) {
            $project->category_id = $project->type->category_id;
        }
        return view('pages.project.show', [
            'project' => new ProjectRepository($project),
            'faker' => \Faker\Factory::create()

        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $repo = new ProjectRepository;
        $project = $repo->find($id);
        if (null != $project->type_id) {
            $project->category_id = $project->type->category_id;
        }

        return view('pages.project.form', [
            'project' => $project,
            'return' => route('project.show', ['id' => $id]),
            'form' => [
                'url' => route('project.update', ['id' => $id]),
                'method' => 'put',
                'name' => 'Update project ' . $project->name,
            ]
        ]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(ProjectRequest $request)
    {

        $project = new ProjectRepository();
        $project->name = $request->name;
        $project->code = $request->code;
        $project->phase = $request->phase;
        $project->customer_id = $request->customer_id;
        $project->pm_id = $request->pm_id;
        $project->csm_id = $request->csm_id;
        $project->started_at = $request->started_at;
        $project->estimated_at = $request->estimated_at;
        $project->complete = $request->complete;
        $project->rag = $request->rag;
        $project->client_contact = $request->client_contact;
        $project->summary = $request->summary;
        $project->actions_nw = $request->actions_nw;
        $project->actions_lw = $request->actions_lw;
        try {
            $project->save();
            if ($project->phase == 'closed') {
                $project->finished_at = $project->now();
                $project->save();
                $project->delete();
            }
            return redirect()->route('project.show', [$project->id]);
        } catch (\Exception $e) {
            return redirect()->route('screen.error')->withErrors(['errors' => $e->getMessage()]);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(ProjectRequest $request, $id)
    {
        $project = ProjectRepository::find($id);
        $project->name = $request->name;
        $project->code = $request->code;
        $project->phase = $request->phase;
        $project->customer_id = $request->customer_id;
        $project->pm_id = $request->pm_id;
        $project->csm_id = $request->csm_id;
        $project->started_at = $request->started_at;
        $project->estimated_at = $request->estimated_at;
        $project->complete = $request->complete;
        $project->rag = $request->rag;
        $project->client_contact = $request->client_contact;
        $project->summary = $request->summary;
        $project->actions_nw = $request->actions_nw;
        $project->actions_lw = $request->actions_lw;

        try {
            $project->save();
            if ($project->phase == 'closed') {
                $project->finished_at = $project->now();
                $project->save();
                $project->delete();
                return redirect()->route('project.index');
            }

            return redirect()->route('project.show', [$project->id]);
        } catch (\Exception $e) {
            return redirect()->route('screen.error')->withErrors(['errors' => $e->getMessage()]);
        }
    }
}
