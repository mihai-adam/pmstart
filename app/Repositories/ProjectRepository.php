<?php
/**
 * Created by PhpStorm.
 * User: madam
 * Date: 10/15/2015
 * Time: 11:28 AM
 */

namespace App\Repositories;

use App\Models\Project;
use App\Repositories\Project\Charts;
use Exception;

class ProjectRepository extends BaseRepository
{

    protected function factory()
    {
        return new Project();
    }

    public function chart()
    {
        return new Charts($this->model());
    }

    public function isClosed()
    {
        return $this->model()->trashed();
    }

    public function getRagIndex($key, $total = false)
    {
        $id = $this->model()->id;

        $target = route('project.show', ['id' => $this->model()->id]) . '#' . $key . '-panel-report';
        $chart = $this->chart();
        $index = $chart->counter($key, 'rag');
        $red = isset($index['red']) ? $index['red'] : 0;
        $amber = isset($index['amber']) ? $index['amber'] : 0;
        $green = isset($index['green']) ? $index['green'] : 0;
        $output = '';
        if ($total) {
            $closed = isset($index['darkgreen']) ? $index['darkgreen'] : 0;
            $output = '<span class="status-green"> ' . $closed . ' </span>|';
        }
        $output .= '<span class="text-success">' . $this->getRagTargetElement($key, $green, 'text-success') . '</span> | ';
        $output .= '<span class="text-warning">' . $this->getRagTargetElement($key, $amber, 'text-warning') . '</span> | ';
        $output .= '<span class="text-danger"> ' . $this->getRagTargetElement($key, $red, 'text-danger') . '</span>';

        return $output;
    }

    public function getRagTargetElement($key, $value, $class)
    {

        if (!$this->isClosed()) {
            $target = route('project.show', ['id' => $this->model()->id]) . '#' . $key . '-panel-report';
            $output = '<a class="' . $class . '"  href="' . $target . '"> ' . $value . ' </a>';
        } else {
            $output = $value;
        }
        return '<strong>' . $output . '</strong>';
    }

}