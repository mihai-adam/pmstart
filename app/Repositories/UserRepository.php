<?php
/**
 * Created by PhpStorm.
 * User: madam
 * Date: 10/15/2015
 * Time: 11:28 AM
 */

namespace App\Repositories;

use App\Models\User;
use Auth;

class UserRepository extends BaseRepository
{

    protected static $instance;
    protected $users;

    protected function factory()
    {
        return new User();
    }

    public static function instance()
    {
        if (null == self::$instance) {
            self::$instance = new UserRepository();
        }
        return self::$instance;
    }

    public function getAll($role = null)
    {
        if (null == $this->users) {
            foreach ($this->model()->select()->orderBy('name', 'ASC')->get() as $user) {
                $this->users[$user->id] = $user;
            }
        }
        if (null != $role) {
            $output = [];
            foreach ($this->users as $id => $user) {
                if ($user->role == $role) {
                    $output[$id] = $user;
                }
            }
            return $output;
        }
        return $this->users;
    }

    public static function getAllRoles($superuser = true)
    {
        $roles = [
            User::USER_ROLE_SUPERUSER,
            User::USER_ROLE_ADMINISTRATOR,
            User::USER_ROLE_CSM,
            User::USER_ROLE_PM
        ];
        if (!$superuser) {
            unset($roles[array_search(User::USER_ROLE_SUPERUSER, $roles)]);
        }
        return $roles;
    }

    public static function myProfile()
    {
        return new self(Auth::user());
    }

    public static function canManageUsers(User $user)
    {
        return (User::USER_ROLE_SUPERUSER == $user->role || User::USER_ROLE_ADMINISTRATOR == $user->role);
    }


}