<?php
/**
 * Created by PhpStorm.
 * User: madam
 * Date: 10/15/2015
 * Time: 11:28 AM
 */

namespace App\Repositories;

use App\Models\Component;

class ComponentRepository extends BaseRepository
{

    protected static $instance;

    protected $components;

    protected function factory()
    {
        return new Component();
    }

    public static function instance()
    {
        if (null == self::$instance) {
            self::$instance = new ComponentRepository();
        }
        return self::$instance;
    }

    public function getAll($component = null)
    {
        if (null == $this->components) {
            foreach ($this->model()->select()->get() as $cmp) {
                $this->components[$cmp->id] = $cmp;
            }
        }
        if (null != $component) {
            $output = [];
            foreach ($this->components as $id => $cmp) {
                if ($cmp->component == $component) {
                    $output[$id] = $cmp;
                }
            }
            return $output;
        }
        return $this->components;
    }

    public function update($model, $data = [])
    {
        foreach ($data as $key => $value) {
            $model->$key = $value;
        }
        $model->save();
        return $model;
    }


    public function create($data = [])
    {
        $model = new Component();
        return $this->update($model, $data);
    }

    public function getId($id)
    {
        $components = $this->getAll();
        if (isset($components[$id])) {
            return $components[$id];
        }
        return null;
    }

    public function getAllRags()
    {
        return $this->getAll(Component::RAG_COMPONENT_KEY);
    }

    public function getRankId($id)
    {
        $src = $this->getId($id);
        if (null != $src) {
            return (Component::RAG_COMPONENT_KEY == $src->component) ? $src : null;
        }
        return null;
    }

    public function createRank($data = [])
    {
        $data['component'] = Component::RANK_COMPONENT_KEY;
        return $this->create($data);
    }

    public function getAllPhases()
    {
        return $this->getAll(Component::PHASE_COMPONENT_KEY);
    }

    public function getPhaseId($id)
    {
        $src = $this->getId($id);
        if (null != $src) {
            return (Component::PHASE_COMPONENT_KEY == $src->component) ? $src : null;
        }
        return null;
    }

    public function createPhase($data = [])
    {
        $data['component'] = Component::PHASE_COMPONENT_KEY;
        return $this->create($data);
    }

    public function getAllCategories()
    {
        return $this->getAll(Component::CATEGORY_COMPONENT_KEY);
    }

    public function getCategoryId($id)
    {
        $src = $this->getId($id);
        if (null != $src) {
            return (Component::CATEGORY_COMPONENT_KEY == $src->component) ? $src : null;
        }
        return null;
    }

    public function createCategory($data = [])
    {
        $data['component'] = Component::CATEGORY_COMPONENT_KEY;
        return $this->create($data);
    }




}