<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;

abstract class Repository
{
    protected $model;


    protected abstract function factory();


    public function __construct(Model $model = null)
    {
        $this->model = (null != $model) ? $model : $this->factory();
    }


    public function model()
    {
        if (null == $this->model) {
            $this->model = $this->factory();
        }
        return $this->model;
    }


    public function __call($name, $args = [])
    {
        return call_user_func_array([$this->model(), $name], $args);
    }

    public function __get($name)
    {
        return $this->model()->$name;
    }

    public static function __callStatic($name, $args = [])
    {
        $class = get_called_class();
        $instance = new $class();
        return $instance->__call($name, $args);
    }

    public function __set($name, $value)
    {
        $this->model()->$name = $value;
    }



}