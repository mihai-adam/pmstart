<?php
/**
 * Created by PhpStorm.
 * User: madam
 * Date: 10/15/2015
 * Time: 11:28 AM
 */

namespace App\Repositories;

use App\Models\Category\Type;
use Exception;

class TypeRepository extends BaseRepository
{

    protected function factory()
    {
        return new Type();
    }


    public function getAll()
    {
        return $this->model()
            ->with('category')
            ->orderBy('name', 'ASC')
            ->get();
    }

    public function getId($id)
    {
        $output = null;
        return $this->with('category')->find($id);

    }

    public function update($id, $data = [])
    {
        $model = $this->getId($id);
        if (null != $model) {
            foreach ($data as $key => $value) {
                $model->$key = $value;
            }
            $model->save();
            return $model;
        } else {
            throw new Exception('Specified type is no longer available');
        }

    }

    public function create($data = [])
    {
        $model = $this->model();
        foreach ($data as $key => $value) {
            $model->$key = $value;
        }
        $model->save();
        return $model;
    }


}