<?php
/**
 * Created by PhpStorm.
 * User: madam
 * Date: 10/15/2015
 * Time: 11:28 AM
 */

namespace App\Repositories;

use App\Models\Category;
use App\Models\Component;
use Exception;

class CategoryRepository extends BaseRepository
{

    protected function factory()
    {
        return new Category();
    }


    public function getAll()
    {
        return $this->model()
            ->with('types')
            ->where('component', Component::CATEGORY_COMPONENT_KEY)
            ->get();
    }

    public function getId($id)
    {
        $output = null;
        $component = $this->with('types')
            ->find($id);
        if (null != $component && $component->component == Component::CATEGORY_COMPONENT_KEY) {
            $output = $component;
        }
        return $output;

    }

    public function update($id, $data = [])
    {
        $model = $this->getId($id);
        if (null != $model) {
            foreach ($data as $key => $value) {
                $model->$key = $value;
            }
            $model->save();
            return $model;
        } else {
            throw new Exception('Specified category is no longer available');
        }

    }


    public function create($data = [])
    {
        $model = new Category();
        foreach ($data as $key => $value) {
            $model->$key = $value;
        }
        $model->save();
        return $model;
    }


}