<?php

/**
 * Created by PhpStorm.
 * User: madam
 * Date: 3/4/2016
 * Time: 11:29 AM
 */
namespace App\Repositories\Project;

use App\Models\Project;

class Charts
{
    private $project;

    public function __construct(Project $project)
    {
        $this->project = $project;
    }

    public function counter($collection, $key, $closed = false)
    {
        $output = [
            'darkgreen' => 0
        ];
        foreach ($this->project->$collection as $raid) {
            if (null != $raid->$key) {
                if ($raid->trashed()) {
                    $output['darkgreen']++;
                } else {
                    if (!isset($output[$raid->$key])) {
                        $output[$raid->$key] = 1;
                    } else {
                        $output[$raid->$key]++;
                    }
                }
            }
        }
        if (!$closed) {
            unset($output['darkgreen']);
        }
        arsort($output);
        return $output;
    }

    private function status($counter)
    {

        $values = array_keys($counter);
        if (!empty($counter)) {
            return $values[0];
        }
        return 'darkgreen';

    }

    private function ragMeter($column, $closed = false)
    {
        $counter = $this->counter($column, 'rag', $closed);
        $output = [
            'status' => $this->status($counter),
            'counter' => []
        ];
        foreach ($counter as $rag => $value) {
            $output['counter'][$rag] = $value;
        }
        return $output;
    }

    public function risks($closed = false)
    {
        return $this->ragMeter('risks', $closed);
    }

    public function issues($closed = false)
    {
        return $this->ragMeter('issues', $closed);
    }

    public function actions($closed = false)
    {
        return $this->ragMeter('actions', $closed);
    }

    public function dependencies($closed = false)
    {
        return $this->ragMeter('dependencies', $closed);
    }

}