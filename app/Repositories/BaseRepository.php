<?php
/**
 * Created by PhpStorm.
 * User: madam
 * Date: 10/15/2015
 * Time: 11:28 AM
 */

namespace App\Repositories;


abstract class BaseRepository extends Repository
{

    public function getId($id)
    {
        $class = get_called_class();
        $repo = new $class();
        $model = $repo->find($id);
        return new $class($model);
    }

    public function obliterate()
    {
        $this->$this->delete();
    }

    public static function exportList($list, $field = 'name')
    {
        $output = [];
        foreach ($list as $id => $element) {
            $output[$id] = $element->$field;
        }
        return $output;
    }
}