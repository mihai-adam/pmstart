<?php
/**
 * Created by PhpStorm.
 * User: madam
 * Date: 10/15/2015
 * Time: 11:28 AM
 */

namespace App\Repositories;

use App\Models\Raid;

class RaidRepository extends BaseRepository
{

    protected function factory()
    {
        return new Raid();
    }


}