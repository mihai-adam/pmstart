var pdt = {
    datatable: {
        element: function (e) {
            var rows = e.attr('data-length');
            if (typeof(rows) == "undefined") {
                rows = 10;
            }
            var table = e.DataTable({
                responsive: true,
                stateSave: true,
                pageLength: rows,
                initComplete: function () {
                    e.attr('style', '');
                }
            });

            return table;
        }
    },

    graph: {
        donut: function (e) {
            $(e).each(function (i, element) {
                sum = 0;
                data = new Array;
                colors = new Array;
                target = $(element).find('span');
                target.each(function (i, e) {
                    sum += parseInt($(e).attr('data-value'));
                });
                target.each(function (i, e) {
                    data.push({
                        value: Math.round(100 * parseInt($(e).attr('data-value')) / sum),
                        label: $(e).html()
                    });
                    colors.push($(e).attr('data-color'));

                });
                target.hide();
                Morris.Donut({
                    resize: false,
                    element: element,
                    data: data,
                    colors: colors,
                    formatter: function (x) {
                        return x + "%"
                    }
                });
            });
        }
    },

    form: {
        actionConfirm: function (element) {
            element.click(function (e) {
                e.preventDefault();
                var target = $(this).attr('data-confirm');
                $(target).find('.modal-target').attr('href', $(this).attr('href'));
                $(target).modal();
            });
        },
        formConfirm: function (element) {
            element.click(function (e) {
                e.preventDefault();
                var target = $(this).attr('data-confirm');
                $(target).find('.modal-action').attr('action', $(this).attr('href'));
                $(target).modal();
            });
        },
        datepicker: function (element) {
            element.datepicker({
                format: "yyyy/mm/dd",
                todayBtn: true,
                calendarWeeks: true,
                autoclose: true
            });
        }

    }
}

$(document).ready(function () {
    pdt.datatable.element($('.datatable'));

    if ($('.donut-graph').length) {
        pdt.graph.donut($('.donut-graph'));
    }
    pdt.form.actionConfirm($('.action-confirm'));
    pdt.form.formConfirm($('.form-confirm'));

    pdt.form.datepicker($('.datepicker'));

    $('a.dt-cc').dtColumnToggler({}, function (element, index, column, table) {
        console.log(element, index, column, table)
        if ('Status' == column) {
            var visibility = table.column(index).visible();
            if (!visibility) {
                table.column(index).search('Active', true, false).draw();
            }
            else {
                table.column(index).search('', true, false).draw();
            }
        }
    });
});