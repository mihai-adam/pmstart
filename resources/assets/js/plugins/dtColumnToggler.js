/**
 * Created by madam on 3/24/2016.
 */
(function ($) {
    $.fn.dtColumnToggler = function (options, callback) {

        // Establish our default settings
        var settings = $.extend({}, options);
        var p = {
            table: function (e) {
                return $(e).DataTable();
            },
            container: function () {
                return $('<ul></ul>')
                    .addClass('dropdown-menu  multi-level')
                    .attr('role', 'menu')
            },
            element: function (index, column, table) {
                return $('<li></li>').append(function () {
                    var visibility = table.column(index).visible();
                    var lnk = $('<a></a>')
                        .addClass('toggle-column prevent-close')
                        .attr('data-target-column', column)
                        .attr('href', '#')
                        .append(function () {
                            var icon = $('<i></i>');
                            if (visibility) {
                                icon.addClass('fa fa-check-square-o');
                            }
                            else {
                                icon.addClass('fa fa-square-o');
                            }
                            return icon;
                        }).append(' ' + column);
                    lnk.on('click', function (e) {
                        e.stopPropagation();
                        e.preventDefault();
                        table.column(index).visible(!table.column(index).visible());
                        var i = $(this).find('i');
                        if (table.column(index).visible()) {
                            i.removeClass('fa-square-o').addClass('fa-check-square-o')
                        }
                        else {
                            i.removeClass('fa-check-square-o').addClass('fa-square-o')
                        }
                        if ('function' == typeof callback) {
                            callback(this, index, column, table);
                        }
                    });
                    return lnk;
                })
            }
        };

        return this.each(function () {
            var table = p.table($(this).attr('data-table-rel'));
            var container = p.container();
            for (var i = 0; i < table.columns().header().length; i++) {
                var column = $(table.column(i).header()).html();
                if ('' != column) {
                    container.append(p.element(i, column, table));
                }
            }
            $(this).after(container);
        });
    }
}(jQuery));
