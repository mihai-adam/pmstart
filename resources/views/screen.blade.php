<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>MSD Dashboard Tool - @yield('title')</title>


    @if(App::environment('local'))
        <link href="/css/assets-scss.css?r={{rand(1000, 9999)}}" rel="stylesheet">
        <link href="/css/assets-dist.css?r={{rand(1000, 9999)}}" rel="stylesheet">
        <link href="/css/application.css?r={{rand(1000, 9999)}}" rel="stylesheet">
    @else
        <link href="/css/style.css" rel="stylesheet">
        @endif

        @yield('styles')
                <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->


</head>

<body class="@yield('class')" id="@yield('id')" data-spy="scroll" data-target=".navbar" data-offset="200">
@yield('body')
@if(App::environment('local'))
    <script src="/js/assets.js?r={{rand(1000, 9999)}}"></script>

    <script src="/js/document.js?r={{rand(1000, 9999)}}"></script>
    <script src="/js/dev.js"></script>

@else
    <script src="/js/main.js"></script>
@endif

@yield('scripts')

</body>

</html>
