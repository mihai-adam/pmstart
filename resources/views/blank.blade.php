@extends('screen')
@section('body')

    <div class="container-fluid" style="margin: auto 150px;">

        <div id="page-wrapper-changed" style="margin-top: 150px">

            <!-- Start content section -->
            @yield('content')
                    <!-- Stop content section -->
        </div>

    </div>
@stop

