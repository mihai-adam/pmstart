<nav class="navbar navbar-default navbar-fixed-bottom" role="navigation" style="min-height:34px; height: 34px"
     xmlns="http://www.w3.org/1999/html">

    <div class=" pull-right" style="padding: 7px">
        &copy; Copyright <span class=" text-primary"><strong>Endava Managed
                Services </strong></span><strong>(MSD)</strong> {{date('Y')}}
    </div>
</nav>