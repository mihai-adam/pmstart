<!-- /.navbar-header -->

<ul class="nav navbar-top-links navbar-right" style="margin-right: 0px;">
    <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            <i class="fa fa-files-o fa-fw"></i> Projects <i class="fa fa-caret-down"></i>
        </a>
        <ul class="dropdown-menu dropdown-projects">
            <li>
                <a href="{{route('project.index')}}">
                    <i class="fa fa-files-o fa-fw"></i> All
                </a>
            </li>
            <li>
                <a href="{{route('project.create')}}">
                    <i class="fa fa fa-plus-square"> </i> Add</a>
            </li>
        </ul>
    </li>
    @if(config('theme.devmenu', false))
        @include('partials.menus.dev')
    @endif
    @can('manage-users')
    <li class="">
        <a href="{{route('user.index')}}">
            <i class="fa fa-users fa-fw"></i> Manage users
        </a>

        <!-- /.dropdown-user -->
    </li>
    @endcan
            <!-- /.dropdown -->
    <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="">
            <i class="fa fa-user fa-fw"></i> User <i class="fa fa-caret-down"></i>
        </a>
        <ul class="dropdown-menu dropdown-user">
            <li><a href="{{route('user.editMyProfile')}}"><i class="fa fa-user fa-fw"></i> Profile</a></li>
            <li><a href="{{route('user.editPassword')}}"><i class="fa fa-lock fa-fw"></i> Change password</a></li>
            <li class="divider"></li>
            <li><a href="{{route('user.logout')}}"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
            </li>
        </ul>
        <!-- /.dropdown-user -->
    </li>
    <!-- /.dropdown -->

    <li class="">
        <a href="#">
            <i class="fa fa-arrow-circle-o-up fa-lg"></i>
        </a>

        <!-- /.dropdown-user -->
    </li>
</ul>
<!-- /.navbar-top-links -->