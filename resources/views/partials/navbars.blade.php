<!-- Navigation -->
<nav class="navbar navbar-static-top navbar-default" data-spy="affix" data-offset-top="200"
     style="background-color:#fff">
    <div class="navbar-header">

        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">

            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
            <img class="pull-left" src="/images/logo.iix" style="height: 40px; margin: 4px 25px 4px 4px;">
        <a class="pull-left" href="{{route('screen.dashboard')}}">
            <img src="/images/pmSTART.jpg" style="height: 40px; margin: 4px 25px 4px 4px;"></a>
        <a class="navbar-brand" href="{{route('screen.dashboard')}}">{{config('app.name')}}</a>
        <a class="navbar-brand" href="{{route('screen.dashboard')}}"><span class="text-primary"> @yield('title')</span>
        </a>
    </div>

    @include('partials.menus.top')

</nav>
<!-- ./ Navigation -->