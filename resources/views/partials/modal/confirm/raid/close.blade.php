<div class="modal fade" tabindex="-1" role="dialog" id="close-modal">
    {!! Form::open(['url'=>'', 'method' => 'delete', 'class'=>'modal-action'])!!}


    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Close RAID </h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <p class="col-md-12">Are you sure yow want to close the RAID record?</p>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button class="btn btn-primary">Close RAID</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    {!! Form::close() !!}
            <!-- /.modal-dialog -->
</div><!-- /.modal -->