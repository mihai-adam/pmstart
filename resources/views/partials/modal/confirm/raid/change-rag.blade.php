<div class="modal fade" tabindex="-1" role="dialog" id="change-rag-modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Change RAG Status </h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <p class="col-md-12">Are you sure yow want to change RAG Status ?</p>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <a class="btn btn-primary modal-target" href="#">Change</a>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div><!-- /.modal -->