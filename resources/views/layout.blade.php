@extends('screen')
@section('body')
    @include('partials.navbars')

    <div class="container-fluid container-psdt">

        <!-- Start content section -->
        @yield('content')
                <!-- Stop content section -->

        <div class="row">
            @include('partials.footer')
                    <!-- /#page-wrapper -->
        </div>
    </div>
@stop

