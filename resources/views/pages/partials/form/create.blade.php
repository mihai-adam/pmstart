<div class="col-lg-12">
    @if(isset($return))
        <div class="text-left pull-left">
            <a type="button" class="btn btn-primary" href="{{$return}}"><i class="fa fa-backward"> </i> Back</a>
        </div>
    @endif
    <div class="text-right">
        <button type="reset" class="btn btn-danger"><i class="fa fa-remove"> </i> Reset</button>
        <button type="submit" class="btn btn-info"><i class="fa fa-save"> </i> Save</button>
    </div>
</div>