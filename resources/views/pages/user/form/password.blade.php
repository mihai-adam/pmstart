@extends('layout')
@section('title','Change password')
@section('id','manage-users')
@section('class','manage-users')

@section('content')
        <!-- /.row -->
<div class="row">
    <div class="col-lg-4 col-lg-offset-4">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-12">
                        <h5 class="pull-left"><i class="fa fa-fw fa-lock"> </i> Change password</h5>
                    </div>
                </div>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="row">
                    <form name="user" method="post" action="{{route('user.updatePassword')}}">

                        {{csrf_field()}}

                        <div class="form-group">
                            <div class="col-lg-12">

                                {!! formElementPassword('current', 'Current password: ', '', [
                                    'tooltip'=>'Current password'
                                ], $errors) !!}


                            </div>
                            <div class="col-lg-12">
                                {!! formElementPassword('password', 'New password: ', '', [
                                    'tooltip'=>'New password'
                                ], $errors) !!}

                            </div>

                            <div class="col-lg-12">
                                {!! formElementPassword('password_confirmation', 'Confirm new password: ', '', [
                                    'tooltip'=>'Confirm new password'
                                ], $errors) !!}

                            </div>

                        </div>
                        @include('pages.partials.form.create')
                    </form>

                </div>

            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
@stop