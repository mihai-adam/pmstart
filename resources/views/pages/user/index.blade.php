@extends('layout')
@section('title','Manage users')
@section('id','manage-users')
@section('class','manage-users')

@section('content')
        <!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-12">
                        <h5 class="pull-left"><i class="fa fa-fw fa-file"> </i> List of users</h5>

                        <div class="btn-group btn-group-sm text-right pull-right dropdown">
                            <a class="btn btn-default" href="{{route('user.create')}}">
                                <i class="fa fa fa-plus-square"> </i> Add</a>

                            <a role="button" data-toggle="dropdown" class="btn btn-primary dt-cc"
                               data-table-rel="#user-index-table">
                                <i class="fa fa-columns"> </i> Columns <span class="caret"></span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">

                <div class="dataTable_wrapper">
                    <table class="table table-striped table-bordered table-hover datatable" data-length="25"
                           id="user-index-table" style="display: none">
                        <thead>
                        <tr>
                            <th>Order</th>
                            <th>ID</th>
                            <th data-class="expand">Name</th>
                            <th>Email</th>
                            <th data-hide="phone">Role</th>
                        </tr>
                        </thead>

                        <tbody>
                        <?php $i = 0;?>
                        @foreach($users as $user)
                            <tr class="row-red odd" role="row">
                                <td>{{++$i}}</td>
                                <td><a href="{{route('user.edit', ['id'=>$user->id])}}">{{$user->id}}</a></td>
                                <td><a href="{{route('user.edit', ['id'=>$user->id])}}">{{$user->name}}</a></td>
                                <td>{{$user->email}}</td>
                                <td>{{trans('component.role.'.$user->role)}}</td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
@stop