@extends('layout')
@section('title','Update profile')
@section('id','manage-users')
@section('class','manage-users')

@section('content')
        <!-- /.row -->
<div class="row">
    <div class="col-lg-6 col-lg-offset-3">
        <form name="user" method="post" action="{{$action}}">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-12">
                            <h5 class="pull-left"><i
                                        class="fa fa-fw fa-user"> </i> {{(null != $user->id)? 'Edit user': 'Create new user'}}
                            </h5>
                        </div>
                    </div>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="row">


                        {{csrf_field()}}

                        <div class="form-group">
                            <div class="col-md-6">
                                <!-- Mspp Code element-->
                                {!! formElementText('name', 'Name: ', $user->name, [
                                    'tooltip'=>'Your name',
                                    'placeholder'=>'Name'
                                ], $errors) !!}
                                        <!-- ./Mspp Code-->

                            </div>
                            <div class="col-md-6">
                                <!-- Mspp Code element-->
                                {!! formElementText('email', 'Email: ', $user->email, [
                                    'tooltip'=>'Your email',
                                    'placeholder'=>'Email'
                                ], $errors) !!}
                                        <!-- ./Mspp Code-->

                            </div>
                            @can('manage-users')
                            <div class="col-md-6">
                                <!-- Mspp Code element-->
                                {!! formElementSelect('role', 'Role: ', $user->role,roles(), [
                                    'tooltip'=>'User role'
                                ], $errors) !!}
                                        <!-- ./Mspp Code-->

                            </div>
                            <div class="col-md-6">
                                <!-- Mspp Code element-->
                                {!! formElementSelect('status', 'Status: ', $user->status, [
                                    'active'=>'Active',
                                    'suspended'=>'Suspended'
                                ], [
                                    'tooltip'=>'Status'
                                ], $errors) !!}
                                        <!-- ./Mspp Code-->

                            </div>
                            @endcan
                        </div>


                    </div>

                </div>
                <!-- /.panel-body -->
                <div class="panel-footer">
                    <div class="row">
                        @include('pages.partials.form.create', [
                            'return'=>route('user.index')
                        ])
                    </div>
                </div>
            </div>
        </form>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
@stop