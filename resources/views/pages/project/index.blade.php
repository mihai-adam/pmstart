@extends('layout')
@section('title','Projects')
@section('id','projects')
@section('class','projects')

@section('content')
        <!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-12">
                        <h5 class="pull-left"><i class="fa fa-fw fa-file"> </i> Available projects</h5>

                        <div class="btn-group btn-group-sm text-right pull-right dropdown">
                            <a class="btn btn-default" href="{{route('project.create')}}">
                                <i class="fa fa fa-plus-square"> </i> Add</a>

                            <a role="button" data-toggle="dropdown" class="btn btn-primary dt-cc"
                               data-table-rel="#project-index-table">
                                <i class="fa fa-columns"> </i> Columns <span class="caret"></span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">

                <div class="dataTable_wrapper">
                    <table class="table table-striped table-bordered table-hover datatable" data-length="25"
                           id="project-index-table" style="display: none">
                        <thead>
                        <tr>
                            <th>Order</th>
                            <th>Name</th>
                            <th>Project No.</th>
                            <th>Customer</th>
                            <th>PM</th>
                            <th>CSM</th>
                            <th>Phase</th>
                            <th>Complete</th>
                            <th>Start</th>
                            <th>Finish</th>
                            <th>Last update</th>
                            <th>Modified end date</th>
                            <th>Risks</th>
                            <th>Issues</th>
                            <th>Status</th>
                        </tr>
                        </thead>

                        <tbody>
                        <?php $i = 0;?>
                        @foreach($projects as $project)
                            <?php $project = new \App\Repositories\ProjectRepository($project)?>
                            <tr class="row-red odd" role="row">
                                <td>{{++$i}}</td>
                                <td>
                                    @if(null == $project->deleted_at)
                                        <a href="{{route('project.show', ['id'=>$project->id])}}">{{$project->name}}</a>
                                    @else
                                        {{$project->name}}
                                    @endif
                                </td>
                                <td>{{$project->code}}</td>
                                <td>{{$project->customer->name}}</td>
                                <td>
                                    {{(null != $project->pm) ? $project->pm->name : ''}}
                                </td>
                                <td>
                                    {{(null != $project->csm) ? $project->csm->name : ''}}

                                </td>
                                <td>{{trans('component.phase.'.$project->phase)}}</td>
                                <td>{{$project->complete}}</td>
                                <td>{{$project->date('started_at')}}</td>
                                <td>{{$project->date('estimated_at')}}</td>
                                <td>{{$project->date('updated_at')}}</td>
                                <td class="sorting_1">
                                    @if(count($project->delays))
                                        {{$project->modifiedEndDate}}
                                    @endif
                                </td>
                                <td>
                                    <strong>{!! $project->getRagIndex('risks', false) !!}</strong>
                                </td>
                                <td>
                                    {!! $project->getRagIndex('issues', false) !!}
                                </td>
                                <td>{{(null != $project->deleted_at) ? 'Closed': 'Active'}}</td>


                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
@stop