@extends('pages.project.raid.form')
@section ('elements')
    <div class="col-lg-12">
        <div class="form-group">
            <div class="col-lg-12">
                <!-- estimated_at element-->
                {!! formElementText('project', 'Project name: ', $project->name, [
                    'tooltip'=>'type here',
                    'placeholder'=>'Complete',
                    'readonly'=>'readonly'
                ], $errors) !!}
                        <!-- ./estimated_at-->

            </div>
        </div>
        <div class="form-group">

            <div class="col-lg-6">
                <!-- csm_id element-->
                {!! formElementSelect('rag', 'RAG:', $raid->rag, rags(),[
                    'tooltip'=>'type here'
                ], $errors, true) !!}
                        <!-- ./csm_id-->

            </div>
            <div class="col-lg-6">
                <!-- csm_id element-->
                {!! formElementSelect('probability', 'Probability:', $raid->probability, lmhs(),[
                    'tooltip'=>'type here'
                ], $errors, true) !!}
                        <!-- ./csm_id-->

            </div>
        </div>

        <div class="form-group">
            <div class="col-lg-6">
                <!-- csm_id element-->
                {!! formElementSelect('level', 'Impact ', $raid->impact->level, lmhs(),[
                    'tooltip'=>'type here'
                ], $errors, true) !!}
                        <!-- ./csm_id-->
            </div>
            <div class="col-lg-6">
                <!-- raised_at element-->
                {!! formElementText('raised_at', 'Impact date: ', formdate($raid->impact->raised_at), [
                    'tooltip'=>'type here',
                    'placeholder'=>'Impact date',
                    'class'=>'datepicker'
                ], $errors) !!}
                        <!-- ./raised_at-->
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-4">
                <!-- pm_id element-->
                {!! formElementSelect('author_id', 'Author: ', $raid->author_id, pms(),[
                    'tooltip'=>'type here',
                    'class'=>'chosen-select'
                ], $errors, true) !!}
                        <!-- ./pm_id-->
            </div>
            <div class="col-lg-4">
                <!-- csm_id element-->
                {!! formElementSelect('owner_id', 'Owner ', $raid->owner_id, csms() + pms(),[
                    'tooltip'=>'type here',
                    'class'=>'chosen-select',
                    'sort'=>true
                ], $errors, true) !!}
                        <!-- ./csm_id-->
            </div>
            <div class="col-lg-4">

                <!-- raised_at element-->
                {!! formElementText('estimated_at', 'To be closed by: ', formdate($raid->estimated_at), [
                    'tooltip'=>'type here',
                    'placeholder'=>'To be closed by',
                    'class'=>'datepicker'
                ], $errors) !!}
                        <!-- ./raised_at-->
            </div>
            <div class="col-lg-12">
                <!-- impact_description element-->
                {!! formElementTextarea('description', 'Description: ', $raid->description, [
                    'placeholder'=>'Description'
                ], $errors) !!}
                        <!-- ./impact_description-->
            </div>


            <div class="col-lg-12">
                <!-- impact_description element-->
                {!! formElementTextarea('impact', 'Impact: ', $raid->impact->description, [
                    'placeholder'=>'Impact description'
                ], $errors) !!}
            </div>
            <!-- ./impact_description-->

            <div class="col-lg-12">
                <!-- impact_description element-->
                {!! formElementTextarea('mitigation', 'Mitigation: ', $raid->mitigation, [
                    'placeholder'=>'Mitigation'
                ], $errors) !!}
                        <!-- ./impact_description-->
            </div>

        </div>
    </div>

@stop