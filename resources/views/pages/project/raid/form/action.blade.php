@extends('pages.project.raid.form')
@section ('elements')
    <input type="hidden" name="author_id" value="{{Auth::user()->id}}">
    {{--<input type="hidden" name="rag" value="{{$raid->rag}}">--}}
    <div class="col-lg-12">
        <div class="form-group">
            <div class="col-lg-12">
                <!-- estimated_at element-->
                {!! formElementText('project', 'Project name: ', $project->name, [
                    'tooltip'=>'type here',
                    'placeholder'=>'Complete',
                    'readonly'=>'readonly'
                ], $errors) !!}
                        <!-- ./estimated_at-->

            </div>
        </div>

        <div class="form-group">
            <div class="col-lg-4">
                <!-- csm_id element-->
                {!! formElementSelect('priority', 'Priority: ', $raid->priority, lmhs(),[
                    'tooltip'=>'Priority'
                ], $errors, true) !!}
                        <!-- ./csm_id-->
            </div>
            <div class="col-lg-4">
                <!-- raised_at element-->
                {!! formElementText('estimated_at', 'Agreed completion date: ', formdate($raid->estimated_at), [
                    'tooltip'=>'Agreed completion date',
                    'placeholder'=>'Agreed completion date',
                    'class'=>'datepicker'
                ], $errors) !!}
                        <!-- ./raised_at-->
            </div>
            <div class="col-lg-4">
                <!-- csm_id element-->
                {!! formElementSelect('owner_id', 'Responsible:', $raid->owner_id, csms() + pms(),[
                    'tooltip'=>'Responsible',
                    'class'=>'chosen-select',
                    'sort'=>true
                ], $errors, true) !!}
                        <!-- ./csm_id-->
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-12">
                <!-- impact_description element-->
                {!! formElementTextarea('description', 'Description: ', $raid->description, [
                    'placeholder'=>'Description'
                ], $errors) !!}
                        <!-- ./impact_description-->
            </div>

            <div class="col-lg-12">
                <!-- impact_description element-->
                {!! formElementTextarea('mitigation', 'Update: ', $raid->mitigation, [
                    'placeholder'=>'Update'
                ], $errors) !!}
                        <!-- ./impact_description-->
            </div>

        </div>
    </div>

@stop