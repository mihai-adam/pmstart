@extends('layout')
@section('title', $form['name'])

@section('content')
        <!-- /.row -->
<div class="row">
    <div class="col-lg-6 col-lg-offset-3">
        {!! Form::open(['url'=>$form['url'], 'method' => $form['method']])!!}
        <div class="panel {{_if(empty($errors->all()), 'panel-primary', 'panel-danger')}} ">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-12">
                        <h5 class="pull-left"><i class="fa fa-fw fa-file"> </i> @yield('title')</h5>
                        @include('pages.project.partials.topnav')
                    </div>
                </div>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="row">

                    <input type="hidden" name="type" , value="{{$raid->type}}">
                    @yield('elements')


                </div>
                <!-- /.panel-body -->
            </div>
            <div class="panel-footer">
                <div class="row">
                    <!-- /.panel -->
                    @include('pages.partials.form.create', [
                        'return' =>route('project.show', [$raid->project_id])
                    ])
                </div>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        {!! Form::close() !!}
                <!-- /.row -->
    </div>
</div>
@stop