<!-- /.row -->
@if(!$elements->isEmpty())
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default" id="{{$container}}">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-12">
                            <h5 class="pull-left"><i class="fa fa-fw fa-file"> </i> {{trans('component.raid.'.$type)}}
                            </h5>

                            <div class="btn-group btn-group-sm text-right pull-right padding-7">
                                <a class="btn btn-default" href="{{route('project.raid.create', [
                                    'id'=>$project_id,
                                    'type'=>$type
                                ])}}">
                                    <i class="fa fa fa-plus-square"> </i> Add</a>
                                <a role="button" data-toggle="dropdown" class="btn  btn-xs btn-primary dt-cc"
                                   data-table-rel="#raid-{{$type}}-table">
                                    <i class="fa fa-columns"> </i> Columns <span class="caret"></span>
                                </a>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <table class="table table-striped table-bordered table-hover datatable"
                               id="raid-{{$type}}-table"
                               style="display: none" data-length="10">
                            <thead>
                            <tr>
                                <th>Order</th>
                                <th data-class="expand">Description (What might happen)</th>
                                <th>Impact (What would this impact)</th>
                                <th data-hide="phone">Date raised</th>
                                <th data-hide="phone">Risk Author</th>
                                <th data-hide="phone,tablet">Risk Owner</th>
                                <th data-hide="phone,tablet">Mitigation Actions</th>
                                <th data-hide="phone,tablet">Probability</th>
                                <th data-hide="phone,tablet">Impact</th>
                                <th data-hide="phone,tablet">RAG Status</th>
                                <th data-hide="phone,tablet" data-status="All">Status</th>
                                <th></th>
                            </tr>
                            </thead>

                            <tbody>
                            <?php $i = 0; ?>
                            @foreach($elements as $e)
                                <tr class="{{$e->class}}">
                                    <td>
                                        {{++$i}}
                                    </td>
                                    <td>{{$e->description}}</td>
                                    <td>{{$e->impact->description}}</td>
                                    <td>{{$e->impact->raised_at}}</td>
                                    <td>
                                        {{(null != $e->author) ? $e->author->name : ''}}
                                    </td>
                                    <td>
                                        {{(null != $e->owner) ? $e->owner->name : ''}}
                                    </td>
                                    <td>{{$e->mitigation}}</td>
                                    <td>{{trans('component.lmh.'.$e->probability)}}</td>
                                    <td>{{trans('component.lmh.'.$e->impact->level)}}</td>
                                    <td>{{trans('component.color.'.$e->rag)}}</td>
                                    <td>{{(null != $e->deleted_at)? 'Closed': 'Active'}}</td>
                                    <td class="text-right">
                                        <!-- Single button -->
                                        @if(null == $e->deleted_at)
                                            <div class="btn-group btn-group-xs dropup">
                                                <button type="button" class="btn btn-{{$e->class}} dropdown-toggle"
                                                        data-toggle="dropdown" aria-haspopup="true"
                                                        aria-expanded="false">
                                                    <i class="fa fa-cogs"> </i> <span class="caret"> </span>
                                                </button>
                                                <ul class="dropdown-menu  dropdown-menu-right">
                                                    <li><a href="{{route('project.raid.edit',[
                                                    'id'=>$project_id,
                                                    'raid'=>$e->id
                                               ])}}"><i class="fa fa-pencil"> </i> Edit</a></li>
                                                    <li role="separator" class="divider"></li>
                                                    <li><a class="action-confirm" data-confirm="#change-rag-modal"
                                                           href="{{route('project.raid.rag',[
                                                    'id'=>$project_id,
                                                    'raid'=>$e->id,
                                                    'rag'=>'red'
                                               ])}}"><i class="fa  fa-circle text-danger"> </i> Red</a></li>
                                                    <li><a class="action-confirm" data-confirm="#change-rag-modal"
                                                           href="{{route('project.raid.rag',[
                                                    'id'=>$project_id,
                                                    'raid'=>$e->id,
                                                    'rag'=>'amber'
                                               ])}}"><i class="fa  fa-circle text-warning"> </i> Amber</a></li>
                                                    <li><a class="action-confirm" data-confirm="#change-rag-modal"
                                                           href="{{route('project.raid.rag',[
                                                    'id'=>$project_id,
                                                    'raid'=>$e->id,
                                                    'rag'=>'green'
                                               ])}}"><i class="fa  fa-circle text-success"> </i> Green</a></li>
                                                    @if(!$e->deleted_at != null)
                                                        <li role="separator" class="divider"></li>

                                                        <li><a class="form-confirm" data-confirm="#close-modal" href="{{route('project.raid.destroy',[
                                                    'id'=>$project_id,
                                                    'raid'=>$e->id
                                               ])}}">
                                                                <i class="fa  fa-check"> </i> Close</a>
                                                        </li>
                                                    @endif
                                                </ul>
                                            </div>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
@endif