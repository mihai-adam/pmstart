@if(!$elements->isEmpty())
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default" id="actions-panel-report">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-12">
                            <h5 class="pull-left"><i class="fa fa-fw fa-file"> </i> {{trans('component.raid.action')}}
                            </h5>

                            <div class="btn-group btn-group-sm text-right pull-right padding-7">
                                <a class="btn btn-default" href="{{route('project.raid.create', [
                                    'id'=>$project_id,
                                    'type'=>'action'
                                ])}}">
                                    <i class="fa fa fa-plus-square"> </i> Add</a>
                                <a role="button" data-toggle="dropdown" class="btn  btn-xs btn-primary dt-cc"
                                   data-table-rel="#raid-action-table">
                                    <i class="fa fa-columns"> </i> Columns <span class="caret"></span>
                                </a>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <table class="table table-striped table-bordered table-hover datatable"
                               id="raid-action-table"
                               style="display: none" data-length="10">
                            <thead>
                            <tr>
                                <th>No.</th>
                                <th>Description</th>
                                <th>Date raised</th>
                                <th>Agreed completion date</th>
                                <th>Responsible</th>
                                <th>Update</th>
                                <th>Priority L/M/H</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                            </thead>

                            <tbody>
                            <?php $i = 0; ?>
                            @foreach($elements as $e)
                                <tr class="{{$e->class}}">
                                    <td>
                                        {{++$i}}
                                    </td>
                                    <td>{{$e->description}}</td>
                                    <td>{{$e->created_at}}</td>
                                    <td>{{$e->estimated_at}}</td>
                                    <td>
                                        {{(null != $e->owner) ? $e->owner->name : ''}}
                                    </td>
                                    <td>{{$e->mitigation}}</td>
                                    <td>{{trans('component.lmh.'.$e->priority)}}</td>
                                    <td>{{(null != $e->deleted_at)? 'Closed': 'Active'}}</td>
                                    <td class="text-right">
                                        @if(null == $e->deleted_at)
                                        <!-- Single button -->
                                        <div class="btn-group btn-group-xs dropup">
                                            <button type="button" class="btn btn-{{$e->class}} dropdown-toggle"
                                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fa fa-cogs"> </i> <span class="caret"> </span>
                                            </button>
                                            <ul class="dropdown-menu  dropdown-menu-right">
                                                <li><a href="{{route('project.raid.edit',[
                                                    'id'=>$project_id,
                                                    'raid'=>$e->id
                                               ])}}"><i class="fa fa-pencil"> </i> Edit</a></li>
                                                <li role="separator" class="divider"></li>
                                                <li><a class="action-confirm" data-confirm="#change-rag-modal" href="{{route('project.raid.priority',[
                                                    'id'=>$project_id,
                                                    'raid'=>$e->id,
                                                    'priority'=>'high'
                                               ])}}"><i class="fa  fa-circle text-danger"> </i> High</a></li>
                                                <li><a class="action-confirm" data-confirm="#change-rag-modal" href="{{route('project.raid.priority',[
                                                    'id'=>$project_id,
                                                    'raid'=>$e->id,
                                                    'priority'=>'medium'
                                               ])}}"><i class="fa  fa-circle text-warning"> </i> Medium</a></li>
                                                <li><a class="action-confirm" data-confirm="#change-rag-modal" href="{{route('project.raid.priority',[
                                                    'id'=>$project_id,
                                                    'raid'=>$e->id,
                                                    'priority'=>'low'
                                               ])}}"><i class="fa  fa-circle text-success"> </i> Low</a></li>
                                                @if(!$e->deleted_at != null)
                                                    <li role="separator" class="divider"></li>

                                                    <li><a class="form-confirm" data-confirm="#close-modal" href="{{route('project.raid.destroy',[
                                                    'id'=>$project_id,
                                                    'raid'=>$e->id
                                                    ])}}"> <i class="fa  fa-check"> </i> Close</a>
                                                    </li>
                                                @endif
                                            </ul>
                                        </div>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
@endif