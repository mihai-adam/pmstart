@extends('layout')
@section('title',$project->name)
@section('id','project')
@section('class','project')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading panel-heading padding-bottom-0 padding-top-0">
                    <div class="row">
                        <div class="col-xs-12">
                            <h5><i class="fa fa-fw fa-info"> </i>General information
                                <span class="pull-right text-right">
                                    <i class="fa fa-lg fa-clock-o" data-toggle="tooltip" title="Last update"></i>
                                    <strong>{{$project->date('updated_at')}}</strong>
                                </span></h5>
                        </div>
                    </div>
                </div>
                <!-- widget div-->
                <div class="panel-body">
                    @include('pages.project.show.general', ['project'=>$project])
                </div>
            </div>

        </div>
        <div class="col-lg-12">
            @include('pages.project.show.charts', ['project'=>$project])
        </div>
        <div class="col-lg-12">
            @include('pages.project.partials.risks', [
                'elements'=>$project->risks,
                'type'=>'risk',
                'container'=>'risks-panel-report',
                'project_id'=>$project->id
            ])
        </div>
        <div class="col-lg-12">
            @include('pages.project.partials.issues', [
                'elements'=>$project->issues,
                'type'=>'issue',
                'container'=>'issues-panel-report',
                'project_id'=>$project->id
            ])
        </div>
        <div class="col-lg-12">
            @include('pages.project.partials.dependencies', [
                'elements'=>$project->dependencies,
                'type'=>'dependency',
                'container'=>'dependencies-panel-report',
                'project_id'=>$project->id
            ])
        </div>
        <div class="col-lg-12">
            @include('pages.project.partials.actions', [
                'elements'=>$project->actions,
                'type'=>'action',
                'container'=>'actions-panel-report',
                'project_id'=>$project->id
            ])
        </div>
    </div>
    @include('partials.modal.confirm.raid.change-rag')
    @include('partials.modal.confirm.raid.close')

@stop