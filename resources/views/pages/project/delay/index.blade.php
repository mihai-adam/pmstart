<?php
/**
 * Created by PhpStorm.
 * User: madam
 * Date: 4/7/2016
 * Time: 4:29 PM
 */
$date = new \Carbon\Carbon($estimation)
?>

@foreach($delays as $delay)
    <?php
    $date->addWeekdays($delay->days)
    ?>
    <p>
        <span class="days"><strong>Delay:</strong> {{$delay->days}} working
            @if(1 == $delay->days)
                day;
            @else
                days;
            @endif
        </span>
        <span class="estimation">
            <strong>Estimation:</strong> {{$date->format(\App\Models\Project\Delay::DATE_MASK)}}
        </span>
        <br>
        @if(!empty($delay->category))
            <span class="category-category">
                <strong>Dependency:</strong> {{trans('component.category.dependencies.' . $delay->dependency)}}
                / {{trans('component.category.' . $delay->category)}}
            </span>
            <br>
        @endif


        <span class="reason"><strong>Reason:</strong> {{$delay->reason}}</span>
    </p>
    <hr>
@endforeach