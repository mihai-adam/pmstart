@extends('layout')
@section('title', $form['name'])

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            function switchProjectCategories(dependency, reset) {
                var categories = function () {
                    return {!!  categories(true) !!};
                }();
                var container = $('#category').closest('.el-container');
                if (reset) {
                    $('#category').prop('selectedIndex', 0);
                    container.hide();
                }
                for (var key in categories) {
                    var option = categories[key];
                    if (option["data-related-dependency"] != dependency) {
                        $('#category option[value=' + option["value"] + ']').addClass('hidden');
                    }
                    else {
                        $('#category option[value=' + option["value"] + ']').removeClass('hidden');
                    }
                }
                if ($('#category option:not(.hidden)').length > 1) {
                    container.show();
                }
            }

            $('#dependency').change(function () {
                switchProjectCategories($(this).val(), true)
            });

            var init = $('#category').val();
            switchProjectCategories($('#dependency').val(), true)
            $('#category').val(init);
        });

    </script>
    @stop
    @section('content')
            <!-- /.row -->
    <div class="row">
        <div class="col-lg-4 col-lg-offset-4">
            {!! Form::open(['url'=>$form['url'], 'method' => $form['method']])!!}
            <div class="panel {{_if(empty($errors->all()), 'panel-primary', 'panel-danger')}} ">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-12">
                            <h5 class="pull-left"><i class="fa fa-fw fa-file"> </i> @yield('title')</h5>
                            <div class="btn-group btn-group-sm text-right pull-right padding-7">
                                <a class="btn btn-default" href="{{route('project.show', ['id'=>$delay->project_id])}}">
                                    <i class="fa fa fa-undo"> </i> Back</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="row">

                        <input type="hidden" name="return_to" , value="{{URL::previous()}}">

                        <div class="col-lg-12">
                            <div class="form-group">
                                <div class="col-lg-12">
                                    <!-- raised_at element-->
                                    {!! formElementText('days', 'Days: ', $delay->days, [
                                        'tooltip'=>'type here',
                                        'placeholder'=>'Days',
                                    ], $errors) !!}
                                            <!-- ./raised_at-->
                                </div>
                                <div class="col-lg-12">
                                    <!-- category_id element-->
                                    {!! formElementSelect('dependency', 'Dependency: ', $delay->dependency, dependencies() ,[
                                        'tooltip'=>'type here'
                                    ], $errors, false) !!}
                                            <!-- ./category_id-->
                                </div>
                                <div class="col-lg-12 el-container">
                                    <!-- type_id element-->
                                    {!! formElementSelect('category', 'Category: ', $delay->category, categories(),[
                                    'tooltip'=>'type here'
                                    ], $errors, true) !!}
                                            <!-- ./type_id-->
                                </div>
                                <div class="col-lg-12">
                                    <!-- delay reason-->
                                    {!! formElementTextarea('reason', 'Reason: ', $delay->reason, [
                                        'placeholder'=>'Delay reason',
                                        'rows'=>4
                                    ], $errors) !!}
                                            <!-- ./delay reason-->
                                </div>

                            </div>
                        </div>


                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
                <div class="panel-footer">
                    <div class="row">
                        @include('pages.partials.form.create', [
                            'return'=>route('project.show', ['id'=>$delay->project_id])
                        ])
                    </div>
                </div>
            </div>

            {!! Form::close() !!}
                    <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
@stop


