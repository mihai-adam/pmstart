<div class="row">
    {{--<div class="col-lg-4">--}}
    {{--@include('pages.project.show.chart.panel', ['title'=>'Completion', 'data'=>[--}}
    {{--['text'=>'Incomplete', 'value'=>100-$project->complete, 'color'=>'red'],--}}
    {{--['text'=>'Complete', 'value'=>$project->complete, 'color'=>'green']--}}
    {{--]])--}}
    {{--</div>--}}
    @if(!empty($project->chart()->risks()['counter']))
        <div class="col-lg-3">
            @include('pages.project.show.chart.panel', [
                'title'=>'Risks',
                'data'=>$project->chart()->risks(),
                'component'=>'risk',
                'container'=>'risks-panel-report',
                'counter' =>  $project->getRagIndex('risks', false)
            ])
        </div>
    @endif
    @if(!empty($project->chart()->issues()['counter']))
        <div class="col-lg-3">
            @include('pages.project.show.chart.panel', [
                'title'=>'Issues',
                'data'=>$project->chart()->issues(),
                'component'=>'issue',
                'container'=>'issues-panel-report',
                'counter' =>  $project->getRagIndex('issues', false)
             ])
        </div>
    @endif
    @if(!empty($project->chart()->dependencies()['counter']))
        <div class="col-lg-3">
            @include('pages.project.show.chart.panel', [
                'title'=>'Dependencies',
                'data'=>$project->chart()->dependencies(),
                'component'=>'dependency',
                'container'=>'dependencies-panel-report',
                'counter' =>  $project->getRagIndex('dependencies', false)
            ])
        </div>
    @endif

    @if(!empty($project->chart()->actions()['counter']))
        <div class="col-lg-3">
            @include('pages.project.show.chart.panel', [
                'title'=>'Actions',
                'data'=>$project->chart()->actions(),
                'component'=>'action',
                'container'=>'actions-panel-report',
                'counter' =>  $project->getRagIndex('actions', false)
             ])
        </div>
    @endif
    {{--<div class="col-lg-4">--}}
    {{--@include('pages.project.show.chart.panel', ['title'=>'Open Actions', 'data'=>[--}}
    {{--['text'=>'Blue', 'value'=>rand(0,100), 'color'=>'blue'],--}}
    {{--['text'=>'Green', 'value'=>rand(0,100), 'color'=>'green']--}}
    {{--]])--}}
    {{--</div>--}}

    {{--<div class="col-lg-4">--}}
    {{--@include('pages.project.show.chart.panel', ['title'=>'Open Changes', 'data'=>[--}}
    {{--['text'=>'Blue', 'value'=>rand(0,100), 'color'=>'blue'],--}}
    {{--['text'=>'Green', 'value'=>rand(0,100), 'color'=>'green']--}}
    {{--]])--}}
    {{--</div>--}}
    {{--<div class="col-lg-4">--}}
    {{--@include('pages.project.show.chart.panel', ['title'=>'Open CR', 'data'=>[--}}
    {{--['text'=>'Blue', 'value'=>rand(0,100), 'color'=>'blue'],--}}
    {{--['text'=>'Green', 'value'=>rand(0,100), 'color'=>'green']--}}
    {{--]])--}}
    {{--</div>--}}

</div>