<table class="table table-responsive">
    <tbody>
    <tr>
        <td class="col-sm-6">Project No.:</td>
        <td>{{$project->code}}</td>
    </tr>
    <tr>
        <td>Phase:</td>
        <td>{{trans('component.phase.'.$project->phase)}}</td>
    </tr>
    <tr>
        <td>Start date:</td>
        <td>{{$project->date('started_at')}}</td>
    </tr>
    <tr>
        <td>End date:</td>
        <td>{{$project->date('estimated_at')}}</td>
    </tr>
    <tr>
    @if(!$project->delays->isEmpty())
        <tr>
            <td class="col-sm-6">Modified end date:</td>
            <td>
                {{$project->modifiedEndDate}}
                <a data-toggle="modal" data-target="#delays"><i class="fa fa-question-circle"> </i></a>
            </td>
        </tr>
        <tr>
            <td class="col-sm-6">Reason for modified end date:</td>
            <td>
                {{$project->delays->first()->reason}}
                <a data-toggle="modal" data-target="#delays"><i class="fa fa-question-circle"> </i></a>
            </td>
        </tr>
    @endif
    <tr>
        <td class="col-sm-6">Customer:</td>
        <td>{{$project->customer->name}}</td>
    </tr>
    <tr>
        <td class="col-sm-6">CSM:</td>
        <td> {{(null != $project->csm) ? $project->csm->name : ''}}</td>
    </tr>
    <tr>
        <td>Client contact:</td>
        <td>{{$project->client_contact}}</td>
    </tr>
    <tr>
        <td>Project Manager:</td>
        <td> {{(null != $project->pm) ? $project->pm->name : ''}}</td>
    </tr>
    <tr>
        <td>Overall project status:</td>
        <td>
            @if ($project->rag == 'red')
                <span class="text-danger">
                            <i class="fa fa-lg fa-circle"></i> {{trans('component.color.'.$project->rag)}}
                        </span>
            @elseif($project->rag == 'amber')
                <span class="text-warning">
                            <i class="fa fa-lg fa-circle"></i> {{trans('component.color.'.$project->rag)}}
                        </span>
            @else
                <span class="text-success">
                            <i class="fa fa-lg fa-circle"></i> {{trans('component.color.'.$project->rag)}}
                        </span>
            @endif
        </td>
    </tr>
    <tr>
        <td>Executive summary:
        </td>
        <td>{{$project->summary}}</td>
    </tr>
    <tr>
        <td>Actions performed during last week:
        </td>
        <td>{{$project->actions_lw}}</td>
    </tr>
    <tr>
        <td>Actions planned for next week:
        </td>
        <td>{{$project->actions_nw}}</td>
    </tr>
    <tr>
        <td>

        </td>
        <td>
            <div class="btn-group btn-group-sm pull-right">

                <div class="dropdown">
                    <a role="button" class="btn btn-default btn-sm"
                       href="{{route('project.edit', ['id'=>$project->id])}}">
                        <i class="fa fa fa-pencil"> </i> Edit</a>
                    <button class="btn btn-primary btn-sm" type="button" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false"><i class="fa fa-file"> </i> Create <span class="caret"> </span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dLabel">

                        <li><a href="{{route('project.delay.create', [
                                    'id'=>$project->id
                                ])}}"> Delay</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="{{route('project.raid.create', [
                                    'id'=>$project->id,
                                    'type'=>'risk'
                                ])}}"> Risk</a></li>
                        <li><a href="{{route('project.raid.create', [
                                    'id'=>$project->id,
                                    'type'=>'issue'
                                ])}}"> Issue</a></li>

                        <li><a href="{{route('project.raid.create', [
                                    'id'=>$project->id,
                                    'type'=>'dependency'
                                ])}}"> Dependency</a></li>
                        <li><a href="{{route('project.raid.create', [
                                    'id'=>$project->id,
                                    'type'=>'action'
                                ])}}"> Action</a></li>
                    </ul>

                </div>
            </div>
        </td>
    </tr>
    </tbody>
</table>


<!-- Modal -->
<div class="modal fade" id="delays" tabindex="-1" role="dialog" aria-labelledby="delays">
    <div class="modal-dialog modal-primary" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="delays-title">Project delays</h4>
            </div>
            <div class="modal-body">
                @include('pages.project.delay.index', ['delays'=>$project->delays, 'estimation'=>$project->estimated_at])
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary " data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>