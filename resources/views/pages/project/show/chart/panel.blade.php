<div class="panel {{config('theme.panels.'.$data['status'])}}">
    <div class="panel-heading panel-heading">
        <div class="row">
            <div class="col-xs-8">
                <a href="#{{$container}}"><h4><i class="fa fa-fw fa-bar-chart"> </i> {{$title}}</h4></a>

            </div>
            <div class="col-xs-4 text-right">
                <h4> {!! $counter !!}</h4>
            </div>
        </div>
    </div>
    <!-- widget div-->
    <div class="panel-body">
        <div class="chart donut-graph">
            @foreach($data['counter'] as $key=>$value)
                <span data-value="{{$value}}" data-color="{{config('theme.colors.'. $key)}}">
                    {{trans('component.color.'.$key)}}
                </span>
            @endforeach
        </div>
    </div>
</div>