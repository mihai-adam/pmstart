@extends('layout')
@section('title', $form['name'])
@section('content')
        <!-- /.row -->
<div class="row">
    <div class="col-lg-6 col-lg-offset-3">
        {!! Form::open(['url'=>$form['url'], 'method' => $form['method']])!!}
        <div class="panel {{_if(empty($errors->all()), 'panel-primary', 'panel-danger')}} ">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-12">
                        <h5 class="pull-left"><i class="fa fa-fw fa-file"> </i> @yield('title')</h5>
                        @include('pages.project.partials.topnav')
                    </div>
                </div>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="row">

                    <input type="hidden" name="return_to" , value="{{URL::previous()}}">

                    <div class="col-lg-12">
                        <div class="form-group">
                            <div class="col-lg-12">
                                <!-- Name element-->
                                {!! formElementText('name', 'Project name: ', $project->name, [
                                    'tooltip'=>'type here',
                                    'placeholder'=>'Project name'
                                ], $errors) !!}
                                        <!-- ./Name element-->
                            </div>
                        </div>
                        <div class="form-group ">
                            <div class="col-lg-6">
                                <!-- Mspp Code element-->
                                {!! formElementText('code', 'MSPP Code: ', $project->code, [
                                    'tooltip'=>'type here',
                                    'placeholder'=>'MSPP Code'
                                ], $errors) !!}
                                        <!-- ./Mspp Code-->

                            </div>
                            <div class="col-lg-6">
                                <!-- phase_id element-->
                                {!! formElementSelect('phase', 'Phase: ', $project->phase,  phases() ,[
                                    'tooltip'=>'type here'
                                ], $errors, true) !!}
                                        <!-- ./phase_id-->

                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-6">
                                <!-- customer_id element-->
                                {!! formElementSelect('customer_id', 'Customer: ', $project->customer_id, customers(),[
                                    'tooltip'=>'type here',
                                    'class'=>'chosen-select'
                                ], $errors, true) !!}
                                        <!-- ./customer_id-->
                            </div>
                            <div class="col-lg-6">
                                <!-- customer_id element-->
                                {!! formElementText('client_contact', 'Client contact: ', $project->client_contact, [
                                    'tooltip'=>'Client contact email address',
                                    'placeholder'=>'Client contact'
                                ], $errors) !!}
                                        <!-- ./customer_id-->
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-6">
                                <!-- pm_id element-->
                                {!! formElementSelect('pm_id', 'PM: ', $project->pm_id, pms(),[
                                    'tooltip'=>'type here',
                                    'class'=>'chosen-select'
                                ], $errors, true) !!}
                                        <!-- ./pm_id-->
                            </div>
                            <div class="col-lg-6">
                                <!-- csm_id element-->
                                {!! formElementSelect('csm_id', 'CSM ', $project->csm_id, csms(),[
                                    'tooltip'=>'type here',
                                    'class'=>'chosen-select'
                                ], $errors, true) !!}
                                        <!-- ./csm_id-->
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-6">
                                <!-- started_at element-->
                                {!! formElementText('started_at', 'Start date: ', formdate($project->started_at), [
                                    'tooltip'=>'type here',
                                    'placeholder'=>'Start date',
                                    'class'=>'datepicker'
                                ], $errors) !!}
                                        <!-- ./started_at-->


                            </div>
                            <div class="col-lg-6">
                                <!-- estimated_at element-->
                                {!! formElementText('estimated_at', 'Estimation: ', formdate($project->estimated_at), [
                                    'tooltip'=>'type here',
                                    'placeholder'=>'Estimation date',
                                    'class'=>'datepicker'
                                ], $errors) !!}
                                        <!-- ./estimated_at-->


                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-6">
                                <!-- estimated_at element-->
                                {!! formElementText('complete', 'Complete: ', $project->complete, [
                                    'tooltip'=>'type here',
                                    'placeholder'=>'Complete'
                                ], $errors) !!}
                                        <!-- ./estimated_at-->

                            </div>
                            <div class="col-lg-6">
                                <!-- csm_id element-->
                                {!! formElementSelect('rag', 'RAG ', $project->rag, rags(),[
                                    'tooltip'=>'type here'
                                ], $errors, true) !!}
                                        <!-- ./csm_id-->

                            </div>
                            <div class="col-lg-12">
                                {!! formElementTextarea('summary', 'Executive summary: ', $project->summary, [
                                       'placeholder'=>'Executive summary',
                                       'rows'=>4
                                   ], $errors) !!}
                            </div>
                            <div class="col-lg-12">
                                {!! formElementTextarea('actions_lw', 'Actions performed during last week: ', $project->actions_lw, [
                                       'placeholder'=>'Actions performed during last week',
                                       'rows'=>4
                                   ], $errors) !!}
                            </div>
                            <div class="col-lg-12">
                                {!! formElementTextarea('actions_nw', 'Actions planned for next week: ', $project->actions_nw, [
                                       'placeholder'=>'Actions planned for next week',
                                       'rows'=>4
                                   ], $errors) !!}
                            </div>
                        </div>


                    </div>

                </div>


            </div>
            <!-- /.panel-body -->
            <div class="panel-footer">
                <div class="row">
                    @include('pages.partials.form.create', [
                        'return'=>$return
                    ])
                </div>
            </div>
        </div>
        {!! Form::close() !!}
                <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
@stop


