@extends('layout')
@section('title', 'Fatal error')
@section('id', 'fatal-error')
@section('class', 'fatal-error')
@section('scripts')
    <script src="/js/morris-data.js"></script>
@stop

@section('styles')
    <script src="/css/style.css"></script>
@stop
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="text-danger">Sorry! System was unable to handle your request</h1>
            @foreach($errors->all() as $message)
                <p class="text-danger">{{$message}}</p>
            @endforeach

        </div>
    </div>
@stop
