<?php
return [

    'category' => [
        'dependencies' => [
            'none' => 'None',
            'external-dependencies' => 'External dependencies',
            'internal-dependencies' => 'Internal dependencies',
        ],
        'external-dependencies' => 'External dependencies',
        'third-party-deliverables' => 'Third party deliverables',
        'client-sign-off-client-acceptance' => 'Client sign-off/Client acceptance',
        'scope-creep' => 'Scope creep',
        'hardware-failure' => 'Hardware failure',
        'software-failure' => 'Software failure',
        'environment-access' => 'Environment access',
        'environment-security' => 'Environment security',
        'third-party-contacts' => 'Third party contacts',
        'other' => 'Other',
        'procurement' => 'Procurement',
        'internal-dependencies' => 'Internal dependencies',
        'resource-skills' => 'Resource skills',
        'internal-processes' => 'Internal processes',
        'project-governance' => 'Project governance',
        'documentation' => 'Documentation',
        'service-transition' => 'Service transition',
        'project-approach' => 'Project approach',
        'resources-availability' => 'Resources availability',
    ],

    'phase' => [
        'closing' => 'Closing',
        'implementation' => 'Implementation',
        'kick-off' => 'Kick Off',
        'roll-out' => 'Roll-out',
        'testing' => 'Testing',
        'closed' => 'Closed',
    ],
    'color' => [
        'red' => 'Red',
        'amber' => 'Amber',
        'green' => 'Green',
        'blue' => 'Blue',
        'darkgreen' => 'Closed',
    ],
    'lmh' => [
        'low' => 'Low',
        'medium' => 'Medium',
        'high' => 'High',
        'high' => 'High',
    ],
    'raid' => [
        'risk' => 'Risk',
        'action' => 'Action',
        'issue' => 'Issue',
        'dependency' => 'Dependency'
    ],
    'role' => [
        'super' => 'Super user',
        'admin' => 'Administrator',
        'csm' => 'CSM',
        'pm' => 'PM'
    ],
    'status' => [
        'active' => 'Active',
        'suspended' => 'Suspended'
    ]
];