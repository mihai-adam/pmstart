<?php

use Illuminate\Database\Seeder;
use App\Models\Customers;
use Carbon\Carbon;

class CustomersV1TableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $model = new Customers();
        foreach ($this->data() as $customer) {
            $model->insert([
                'name' => $customer,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }
        $bae = Customers::where('name', 'BEA')->first();
        $bae->name = 'BAE';
        $bae->save();

        $dava = Customers::where('name', 'Endava')->first();
        $dava->name = 'Endava Group';
        $dava->save();
    }

    private function data()
    {
        return [
            'TMG',
            'Visa',
            'R&A',
            'AJG',
            'Endava MSD',
            'BTIG'
        ];
    }
}
