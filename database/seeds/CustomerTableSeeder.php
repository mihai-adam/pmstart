<?php

use Illuminate\Database\Seeder;
use App\Models\Customers;
use Faker\Factory as Faker;
use Carbon\Carbon;

class CustomerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $model = new Customers();
        $model->truncate();

        foreach ($this->data() as $user) {
            $model->insert($user);
        }
    }

    private function data()
    {
        $output = [[
            'name' => 'AMS',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ], [
            'name' => 'BEA',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ], [
            'name' => 'Endava',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ], [
            'name' => 'PC',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]];

        return $output;
    }
}
