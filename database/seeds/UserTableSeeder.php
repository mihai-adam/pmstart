<?php

use Illuminate\Database\Seeder;
use App\Repositories\UserRepository;
use App\Models\User;
use \Hash;
use Carbon\Carbon;
use Faker\Factory as Faker;

class UserTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $model = new UserRepository();
        $model->truncate();

        foreach ($this->data() as $user) {
            $model->insert($user);
        }
    }

    public function data()
    {
        $output = [];
        foreach ($this->users() as $user) {
            $output[] = [
                'name' => $user['name'],
                'email' => strtolower(str_replace(' ', '.', $user['name'])) . '@endava.com',
                'password' => Hash::make('1234'),
                'role' => $user['role'],
                'created_at' => Carbon::now()
            ];
        }
        return $output;
    }

    public function users()
    {
        $output = [
            ['name' => 'super1', 'role' => User::USER_ROLE_SUPERUSER],
            ['name' => 'super2', 'role' => User::USER_ROLE_SUPERUSER],
            ['name' => 'admin1', 'role' => User::USER_ROLE_ADMINISTRATOR],
            ['name' => 'admin2', 'role' => User::USER_ROLE_ADMINISTRATOR],
            ['name' => 'csm1', 'role' => User::USER_ROLE_CSM],
            ['name' => 'csm2', 'role' => User::USER_ROLE_CSM],
            ['name' => 'pm1', 'role' => User::USER_ROLE_PM],
            ['name' => 'pm2', 'role' => User::USER_ROLE_PM],


        ];
        $roles = UserRepository::getAllRoles();
        $faker = Faker::create('ro');
        for ($i = 0; $i < 60; $i++) {
            $role = $roles[rand(0, count($roles) - 1)];
            $output[] = [
                'name' => $faker->name,
                'role' => $role
            ];
        }

        return $output;


    }
}
