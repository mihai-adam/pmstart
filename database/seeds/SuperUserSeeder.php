<?php

use Illuminate\Database\Seeder;
use App\Repositories\UserRepository;
use App\Models\User;
use Carbon\Carbon;

class SuperUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $model = new UserRepository();
        $model->truncate();
        $model->insert([
            'name' => 'Mihai Adam',
            'email' => 'mihai.adam@endava.com',
            'password' => Hash::make('1234'),
            'role' => User::USER_ROLE_SUPERUSER,
            'created_at' => Carbon::now()
        ]);

    }


}
