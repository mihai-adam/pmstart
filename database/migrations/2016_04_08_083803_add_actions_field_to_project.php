<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Project;
class AddActionsFieldToProject extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $project = new Project();
        Schema::table($project->getTable(), function ($table) {
            $table->text('actions_lw')->after('summary')->nullable()->default(null);
            $table->text('actions_nw')->after('actions_lw')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $project = new Project();
        Schema::table($project->getTable(), function ($table) {
            $table->dropColumn('actions_lw');
            $table->dropColumn('actions_nw');
        });
    }
}
