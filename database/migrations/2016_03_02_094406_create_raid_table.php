<?php

/**
 * Risks, Issues and Dependencies
 *
 * no
 * description => description
 * impact (What would this impact) => impact
 *
 * days to go (to be calculated)
 * author=> author_id
 * Risk/Issue Owner =>owner_id
 * Probability =>probability
 * Mitigation actions => mitigation
 * Overall status =>status_id
 *
 * Impact =>impact
 * Date raised => created_at
 * Impact Date =>impacted_at
 * To be closed by =>closed_at
 * Risk status => soft-delete => deleted_at
 */
/**
 * Actions
 * no
 * description=>description
 * Responsible =>owner_id
 * Update (has many updates)
 * Priority => priority
 * date raised=>created_at
 * Agreed completion date=>estimated_at
 * To be closed by =>closed_at
 * Action status soft-delete => deleted_at
 */


use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Repositories\RaidRepository as Raid;

class CreateRaidTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $repo = new Raid();
        Schema::create($repo->model()->getTable(), function (Blueprint $table) use ($repo) {
            $table->increments('id');
            $table->integer('project_id')->unsigned();
            $table->integer('author_id')->unsigned();
            $table->integer('owner_id')->unsigned();
            $table->integer('impact_id')->unsigned()->nullable()->default(NULL)->comment = 'for all types except action';
            $table->string('probability', 16)->nullable()->default(NULL)->comment = 'lmh for all types except action';
            $table->string('priority', 16)->nullable()->default(NULL)->comment = 'lmh for action type';
            $table->string('type', 16)->comment = "risk, action, issue or dependency";
            $table->string('rag')->comment = 'for all types except action';
            $table->string('description', 256);
            $table->string('mitigation', 256)->nullable()->default(NULL)->comment = 'for all types except action';
            $table->date('estimated_at')->nullable()->default(NULL)->comment = 'for action type';
            $table->timestamps();
            $table->date('closed_at')->nullable()->default(NULL);
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $repo = new Raid();
        Schema::drop($repo->model()->getTable());
    }
}
