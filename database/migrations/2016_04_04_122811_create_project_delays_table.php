<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Project\Delay;
use App\Models\Project;

class CreateProjectDelaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // create delay table
        $model = new Delay();
        Schema::create($model->getTable(), function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id')->unsigned();
            $table->tinyInteger('days')->unsigned();
            $table->string('dependency', 64)->nullable()->default(null);
            $table->string('category', 64)->nullable()->default(null);
            $table->string('reason', 1024)->nullable()->default(null);
            $table->timestamps();
        });
        //remove delay fields from project
        $project = new Project();
        Schema::table($project->getTable(), function ($table) {
            $table->dropColumn('dependency');
            $table->dropColumn('category');
            $table->dropColumn('reason');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //drop delay table
        $model = new Delay();
        Schema::drop($model->getTable());
        //put back delay fields to project table
        $project = new Project();
        Schema::table($project->getTable(), function ($table) {
            $table->string('dependency', 32)->nullable()->default(null)->after('code');
            $table->string('category', 32)->nullable()->default(null)->after('dependency');
            $table->text('reason')->after('complete');
        });
    }
}
