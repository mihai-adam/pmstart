<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Project;

class AddCustomerContactAndSummaryFieldsToProjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $project = new Project();
        Schema::table($project->getTable(), function ($table) {
            $table->string('client_contact', 64)->after('deleted_at')->nullable()->default(null);
            $table->text('summary')->after('client_contact')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $project = new Project();
        Schema::table($project->getTable(), function ($table) {
            $table->dropColumn('client_contact');
            $table->dropColumn('summary');
        });
    }
}
