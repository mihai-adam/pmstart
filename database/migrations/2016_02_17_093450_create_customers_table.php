<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Customers;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $customers = new Customers();
        Schema::create($customers->getTable(), function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 128);
            $table->text('description');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $customers = new Customers();
        Schema::drop($customers->getTable());
    }
}
