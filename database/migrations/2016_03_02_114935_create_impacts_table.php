<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Raid\Impact;

class CreateImpactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $model = new Impact();
        Schema::create($model->getTable(), function (Blueprint $table) {
            $table->increments('id');
            $table->string('level', 16)->comment = "low, medium or high";
            $table->string('description', 256);
            $table->date('raised_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $model = new Impact();
        Schema::drop($model->getTable());
    }
}
