<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id')->nullable()->default(null);
            $table->integer('pm_id')->nullable()->default(null);
            $table->integer('csm_id')->nullable()->default(null);
            $table->string('name', 128);
            $table->string('code', 16);
            $table->string('dependency', 32)->nullable()->default(null);
            $table->string('category', 32)->nullable()->default(null);
            $table->string('phase', 32)->nullable()->default(null);
            $table->string('rag')->nullable()->default(null);
            $table->smallInteger('complete')->default(0);
            $table->text('reason');
            $table->timestamp('started_at');
            $table->timestamp('estimated_at');
            $table->timestamp('finished_at')->nullable()->default(null);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('projects');
    }
}
