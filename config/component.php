<?php
/**
 * Created by PhpStorm.
 * User: madam
 * Date: 3/2/2016
 * Time: 2:25 PM
 */
return [
    'category' => [
        'none' => [],
        'external-dependencies' => [
            'third-party-deliverables',
            'client-sign-off-client-acceptance',
            'scope-creep',
            'hardware-failure',
            'software-failure',
            'environment-access',
            'environment-security',
            'third-party-contacts',
            'other',
            'procurement'
        ],
        'internal-dependencies' => [
            'resource-skills',
            'internal-processes',
            'project-governance',
            'documentation',
            'service-transition',
            'hardware-failure',
            'software-failure',
            'environment-access',
            'project-approach',
            'other',
            'resources-availability'
        ]
    ],
    'phase' => [
        'kick-off',
        'implementation',
        'testing',
        'roll-out',
        'closing',
        'closed'
    ],
    'rag' => [
        'red',
        'amber',
        'green'
    ],
    'lmh' => [
        'low',
        'medium',
        'high'
    ],
    'raid' => [
        'risk',
        'action',
        'issue',
        'dependency'
    ]
];
