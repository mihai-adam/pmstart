<?php
/**
 * Created by PhpStorm.
 * User: madam
 * Date: 3/4/2016
 * Time: 11:47 AM
 */
return [
    'colors' => [
        'red' => '#EE0000',
        'amber' => '#F0AD4E',
        'green' => '#008000',
        'blue' => '#0000FF',
        'darkgreen' => '#0C4711'
    ],
    'panels' => [
        'red' => 'panel-danger',
        'amber' => 'panel-warning',
        'green' => 'panel-success',
        'blue' => 'panel-info',
        'darkgreen' => 'panel-success'
    ],
    'devmenu' => env('DEV-MENU', false)
];