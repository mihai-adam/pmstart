var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */


var paths = {
    bower: function (e) {
        return './node_modules/bower_components/' + e;
    },
    public: function (target) {
        return './public/' + target
    },
    jquery: './node_modules/bower_components/jquery/',
    bootstrap: './node_modules/bower_components/bootstrap-sass/',
    fontawesome: './node_modules/bower_components/font-awesome/',
    dropzone: './node_modules/bower_components/dropzone/',
    sbadmin: './node_modules/bower_components/startbootstrap-sb-admin-2/',
    datepicker: './node_modules/bower_components/bootstrap-sass-datepicker/',
    metismenu: './node_modules/bower_components/metisMenu/',
    datatable: './node_modules/bower_components/datatables/',
}

elixir(function (mix) {

    mix.copy(paths.fontawesome + 'fonts', paths.public('fonts/font-awesome'));

    mix.copy(paths.bootstrap + 'assets/fonts', paths.public('fonts'));
    mix.copy(paths.bower('datatables-plugins/integration/bootstrap/images'), paths.public('images'));
    mix.copy(paths.bower('bootstrap-chosen/*.png'), paths.public('images'));

    mix.sass([
        'assets.scss'
    ], paths.public('css/assets-scss.css'));

    mix.styles([
        'startbootstrap-sb-admin-2/dist/css/sb-admin-2.css',
        'startbootstrap-sb-admin-2/dist/css/timeline.css',
        'metisMenu/src/metisMenu.css',
        'morrisjs/morris.css',
        'datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css',
        'seiyria-bootstrap-slider/dist/css/bootstrap-slider.css'
    ], paths.public('css/assets-dist.css'), paths.bower(''));
    mix.sass([
        'application.scss'
    ], paths.public('css/application.css'));

    mix.styles([
        'css/assets-dist.css',
        'css/assets-scss.css',
        'css/application.css',
    ], paths.public('css/style.css'), paths.public(''));

    mix.scripts([
        'plugins/dtColumnToggler.js',
        'document.js'
    ], paths.public('js/document.js'));

    mix.scripts([
        paths.jquery + 'dist/jquery.js',
        paths.bootstrap + 'assets/javascripts/bootstrap.js',
        paths.datepicker + 'js/bootstrap-sass-datepicker.js',
        paths.bower('startbootstrap-sb-admin-2/dist/js/sb-admin-2.js'),
        paths.bower('metisMenu/src/metisMenu.js'),
        paths.bower('raphael/raphael-min.js'),
        paths.bower('morrisjs/morris.min.js'),
        paths.bower('flot/jquery.flot.js'),
        paths.bower('flot.tooltip/js/jquery.flot.tooltip.js'),
        paths.bower('datatables/media/js/jquery.dataTables.js'),
        paths.bower('datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.js'),
        paths.bower('bootstrap-sass-datepicker/js/bootstrap-sass-datepicker.js'),
        paths.bower('seiyria-bootstrap-slider/src/js/bootstrap-slider.js'),
        paths.bower('chosen/chosen.jquery.min.js')
    ], paths.public('js/assets.js'), paths.bower(''));

    mix.scripts([
        'js/assets.js',
        'js/document.js'
    ], paths.public('js/main.js'), paths.public(''));
});
